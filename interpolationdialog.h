#ifndef INTERPOLATIONDIALOG_H
#define INTERPOLATIONDIALOG_H

#include "sylvester/polynomial.hpp"

#include <QDialog>

#include <QtGnuplot/QtGnuplotWidget.h>
#include <QtGnuplot/QtGnuplotInstance.h>

namespace Ui {
class InterpolationDialog;
}

class InterpolationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InterpolationDialog(QWidget *parent = 0);
    ~InterpolationDialog();

	const softdev::BivariatePolynomial& polynomial() const
	{
		return poly;
	}

protected:
	/**
	 * Event filter to receive click events from Gnuplot
	 */
	bool eventFilter(QObject *obj, QEvent *event);

private slots:
	void setDegree(int degree);

	void redrawPlot();

	void setXAxis(int value);
	void setYAxis(int value);

	void solveInterpolation();

signals:
	void graphModified();

	void interpolationChanged(bool has_solution);

private:
    Ui::InterpolationDialog *ui;
	QtGnuplotInstance* instance;

	int x_axis;
	int y_axis;

	int poly_degree;

	softdev::BivariatePolynomial poly;
};

#endif // INTERPOLATIONDIALOG_H

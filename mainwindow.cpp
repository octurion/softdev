#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDialog>

#include "sylvester/polynomial.hpp"
#include "sylvester/sylvester.hpp"

#include "interpolationdialog.h"
#include "graphvisualizer.h"

#include <random>

#include <iostream>
#include <string>
#include <utility>
#include <vector>

using std::pair;
using std::string;
using std::vector;

using softdev::BivariatePolynomial;
using softdev::CoefficientInfo;
using softdev::EigenSolution;
using softdev::SylvesterMatrix;
using softdev::SylvesterPolynomial;
using softdev::SylvesterPolynomialIssue;
using softdev::Roots;

namespace
{
	const int MAX_TRIES = 4;
}

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
	, solution()
{
    ui->setupUi(this);

	QObject::connect(ui->interpolateFirst, &QPushButton::clicked, this,
		[this] {
			auto dialog = new InterpolationDialog(this);
			dialog->exec();

			if (dialog->result() == QDialog::Accepted) {
				ui->firstPolynomial->setText(
					QString::fromStdString(dialog->polynomial().to_string()));
			}
		});

	QObject::connect(ui->interpolateSecond, &QPushButton::clicked, this,
		[this] {
			auto dialog = new InterpolationDialog(this);
			dialog->exec();

			if (dialog->result() == QDialog::Accepted) {
				ui->secondPolynomial->setText(
					QString::fromStdString(dialog->polynomial().to_string()));
			}
		});

	QObject::connect(ui->visualizeResults, &QPushButton::clicked, this,
		[this] {
			auto dialog = new GraphVisualizer(this);
			dialog->setPolynomialsAndPoints(
				firstPoly.to_gnuplot_string(),
				secondPoly.to_gnuplot_string(),
				initialRoots);

			dialog->exec();
		});

	QObject::connect(ui->visualizeResultsChanged, &QPushButton::clicked, this,
		[this] {
			auto dialog = new GraphVisualizer(this);
			dialog->setPolynomialsAndPoints(
				firstPoly.to_gnuplot_string(),
				secondPoly.to_gnuplot_string(),
				changedRoots);

			dialog->exec();
		});
}

QString MainWindow::issueToString(SylvesterPolynomialIssue issue) const
{
	switch (issue) {
	case SylvesterPolynomialIssue::None:
		return tr("Standard eigenproblem");
	case SylvesterPolynomialIssue::MinCoeffTooSmall:
		return tr("Generalized eigenproblem (minimum singular value too small)");
	case SylvesterPolynomialIssue::KappaTooBig:
		return tr("Generalized eigenproblem (Condition number too big)");
	default:
		return tr("");
	};
}

void MainWindow::setSolutionMethod(SylvesterPolynomialIssue issue)
{
	ui->solutionMethod->setText(issueToString(issue));
}

void MainWindow::varChangeSetSolutionMethod(softdev::SylvesterPolynomialIssue issue)
{
	ui->solutionMethodChanged->setText(issueToString(issue));
}

void MainWindow::setMinimumSingularValue(double value)
{
	ui->minSingularValue->setText(QString::number(value));
}

void MainWindow::setMaximumSingularValue(double value)
{
	ui->maxSingularValue->setText(QString::number(value));
}

void MainWindow::setConditionNumber(double value)
{
	ui->kappaValue->setText(QString::number(value));
}

void MainWindow::varChangeSetMinimumSingularValue(double value)
{
	ui->minSingularValueChanged->setText(QString::number(value));
}

void MainWindow::varChangeSetMaximumSingularValue(double value)
{
	ui->maxSingularValueChanged->setText(QString::number(value));
}

void MainWindow::varChangeSetConditionNumber(double value)
{
	ui->kappaValueChanged->setText(QString::number(value));
}

void MainWindow::varChangeSetParameters(int t_1, int t_2, int t_3, int t_4)
{
	ui->varChangeFraction->setText(QString("(%1 + %2 z) / (%3 + %4 z)")
			.arg(t_1)
			.arg(t_2)
			.arg(t_3)
			.arg(t_4));
}

void MainWindow::varChangeSetFailed()
{
	ui->minSingularValueChanged->setText("");
	ui->maxSingularValueChanged->setText("");
	ui->kappaValueChanged->setText("");

	ui->varChangeFraction->setText("Didn't manage to perform variable change");

	ui->solutionsViewChanged->setRowCount(0);

	emit varChangeSolutionExistenceChanged(false);
}

void MainWindow::setInitialRoots(vector<pair<double, double>> newRoots,
			const BivariatePolynomial& first,
			const BivariatePolynomial& second,
			double epsilon)
{
	initialRoots = std::move(newRoots);

	auto* table = ui->solutionsView;
	table->setRowCount(0);

	for (const auto p : initialRoots) {
		auto row = table->rowCount();
		table->insertRow(row);

		auto* x_item = new QTableWidgetItem;
		x_item->setText(QString::number(p.first));
		table->setItem(row, 0, x_item);

		auto* y_item = new QTableWidgetItem;
		y_item->setText(QString::number(p.second));
		table->setItem(row, 1, y_item);

		auto first_eval = first.evaluate_for(p.first, p.second);
		auto* first_item = new QTableWidgetItem;
		first_item->setText(QString::number(first_eval));
		table->setItem(row, 2, first_item);

		auto second_eval = second.evaluate_for(p.first, p.second);
		auto* second_item = new QTableWidgetItem;
		second_item->setText(QString::number(second_eval));
		table->setItem(row, 3, second_item);

		auto* acceptable_item = new QTableWidgetItem;
		bool acceptable = std::abs(first_eval) < epsilon
			&& std::abs(second_eval) < epsilon;
		acceptable_item->setCheckState(acceptable
				? Qt::CheckState::Checked
				: Qt::CheckState::Unchecked);
		auto flags = acceptable_item->flags() & (~Qt::ItemIsUserCheckable);
		acceptable_item->setFlags(flags);
		table->setItem(row, 4, acceptable_item);
	}

	emit solutionExistenceChanged(!initialRoots.empty());
}

void MainWindow::setChangedRoots(vector<pair<double, double>> newRoots,
			const BivariatePolynomial& first,
			const BivariatePolynomial& second,
			double epsilon)
{
	changedRoots = std::move(newRoots);

	auto* table = ui->solutionsViewChanged;
	table->setRowCount(0);

	for (const auto p : changedRoots) {
		auto row = table->rowCount();
		table->insertRow(row);

		auto* x_item = new QTableWidgetItem;
		x_item->setText(QString::number(p.first));
		table->setItem(row, 0, x_item);

		auto* y_item = new QTableWidgetItem;
		y_item->setText(QString::number(p.second));
		table->setItem(row, 1, y_item);

		auto first_eval = first.evaluate_for(p.first, p.second);
		auto* first_item = new QTableWidgetItem;
		first_item->setText(QString::number(first_eval));
		table->setItem(row, 2, first_item);

		auto second_eval = second.evaluate_for(p.first, p.second);
		auto* second_item = new QTableWidgetItem;
		second_item->setText(QString::number(second_eval));
		table->setItem(row, 3, second_item);

		auto* acceptable_item = new QTableWidgetItem;
		bool acceptable = std::abs(first_eval) < epsilon
			&& std::abs(second_eval) < epsilon;
		acceptable_item->setCheckState(acceptable
				? Qt::CheckState::Checked
				: Qt::CheckState::Unchecked);
		auto flags = acceptable_item->flags() & (~Qt::ItemIsUserCheckable);
		acceptable_item->setFlags(flags);
		table->setItem(row, 4, acceptable_item);
	}

	emit varChangeSolutionExistenceChanged(!changedRoots.empty());
}

void MainWindow::readPolynomialsFromFile()
{
	auto file_name = QFileDialog::getOpenFileName(this,
			tr("Open File"), QString(), tr("All Files (*.*)"));

	if (file_name.isEmpty()) {
		return;
	}

	QFile file(file_name);
	if (!file.open(QIODevice::ReadOnly)) {
		QMessageBox::critical(this,
				tr("Coundn't open the file specified"),
				tr("For some reason or other the file you specified couldn't be opened."),
				QMessageBox::Ok);

		return;
	}

	QTextStream stream(&file);
	QString first;
	if (!stream.atEnd()) {
		first = stream.readLine();
	}

	QString second;
	if (!stream.atEnd()) {
		second = stream.readLine();
	}

	if (first.isEmpty() || second.isEmpty()) {
		QMessageBox::critical(this,
				tr("The file contents are malformed"),
				tr("Your file should have the two polynomials in the first two lines."),
				QMessageBox::Ok);

		return;
	}

	ui->firstPolynomial->setText(first);
	ui->secondPolynomial->setText(second);
}

void MainWindow::solveSystem()
{
	BivariatePolynomial first_poly;
	BivariatePolynomial second_poly;

	try {
		string first_str = ui->firstPolynomial->text().toUtf8().constData();
		first_poly = BivariatePolynomial::parse(first_str.c_str());
	} catch(std::exception& e) {
		QMessageBox::critical(this,
				tr("Error while parsing the first polynomial"),
				tr("Your first polynomial is not properly formed."),
				QMessageBox::Ok);

		return;
	}

	try {
		string second_str = ui->secondPolynomial->text().toUtf8().constData();
		second_poly = BivariatePolynomial::parse(second_str.c_str());
	} catch(std::exception& e) {
		QMessageBox::critical(this,
				tr("Error while parsing the second polynomial"),
				tr("Your second polynomial is not properly formed."),
				QMessageBox::Ok);

		return;
	}

	bool converted_successfully;
	auto bound_exp = ui->boundExponent->text().toDouble(&converted_successfully);
	if (!converted_successfully) {
		QMessageBox::critical(this,
				tr("Error while parsing the bound exponent"),
				tr("Your bound exponent is malformed."),
				QMessageBox::Ok);

		return;
	}

	auto root_epsilon = ui->polynomialRootEpsilon->text().toDouble(&converted_successfully);
	if (!converted_successfully) {
		QMessageBox::critical(this,
				tr("Error while parsing the polynomial root epsilon"),
				tr("Your polynomial closeness to zero epsilon is malformed."),
				QMessageBox::Ok);

		return;
	}

	auto eigenvalue_epsilon = ui->eigenvalueEqualityEpsilon->text().toDouble(&converted_successfully);
	if (!converted_successfully) {
		QMessageBox::critical(this,
				tr("Error while parsing the eigenvalue equality epsilon"),
				tr("Your eigenvalue equality epsilon is malformed."),
				QMessageBox::Ok);

		return;
	}

	CoefficientInfo coefficient_info;
	SylvesterPolynomialIssue issue;

	SylvesterPolynomial sylvester({});
	try {
		sylvester = SylvesterPolynomial::from_sylvester_matrix(
				SylvesterMatrix::from_polynomials(first_poly, second_poly));
		coefficient_info = sylvester_polynomial_info(sylvester);

		auto solutions = sylvester_polynomial_eigensolution(sylvester, coefficient_info, bound_exp);
		issue = solutions.issue;
		auto roots = Roots::from_eigenvectors(solutions, eigenvalue_epsilon)
			.root_pairs(first_poly, second_poly, sylvester, root_epsilon);

		setMinimumSingularValue(coefficient_info.min_coefficient());
		setMaximumSingularValue(coefficient_info.max_coefficient());
		setConditionNumber(coefficient_info.kappa());

		setSolutionMethod(issue);

		setInitialRoots(std::move(roots), first_poly, second_poly, root_epsilon);

	} catch (...) {
		QMessageBox::critical(this,
				tr("Couldn't solve the system"),
				tr("For some reason it was not possible to solve the system."),
				QMessageBox::Ok);
		return;
	}

	std::uniform_int_distribution<int> distribution(1, 10);
	std::random_device dev;

	std::mt19937 rng(dev());

	bool var_change_succeeded = false;
	for (int i = 0; i < MAX_TRIES; i++) {
		int t_1 = distribution(rng);
		int t_2 = distribution(rng);
		int t_3 = distribution(rng);
		int t_4 = distribution(rng);

		SylvesterPolynomial new_poly = sylvester.var_change(t_1, t_2, t_3, t_4);

		auto new_coeff_info = softdev::sylvester_polynomial_info(new_poly);
		if (new_coeff_info.kappa() >= coefficient_info.kappa()
				|| log10(new_coeff_info.min_coefficient()) < -2 * bound_exp) {
			continue;
		}
		try {
			auto new_solutions = softdev::sylvester_polynomial_eigensolution(
					new_poly, new_coeff_info, bound_exp);

			new_solutions = softdev::revert_variable_change(
					std::move(new_solutions), t_1, t_2, t_3, t_4);

			auto new_roots = Roots::from_eigenvectors(new_solutions, eigenvalue_epsilon)
				.root_pairs(first_poly, second_poly, new_poly, root_epsilon);

			varChangeSetMinimumSingularValue(new_coeff_info.min_coefficient());
			varChangeSetMaximumSingularValue(new_coeff_info.max_coefficient());
			varChangeSetConditionNumber(new_coeff_info.kappa());

			varChangeSetParameters(t_1, t_2, t_3, t_4);

			varChangeSetSolutionMethod(new_solutions.issue);

			setChangedRoots(std::move(new_roots), first_poly, second_poly,
					root_epsilon);

			var_change_succeeded = true;
			break;
		} catch (...) {
			continue;
		}
	}

	if (!var_change_succeeded) {
		varChangeSetFailed();
	}

	this->firstPoly = std::move(first_poly);
	this->secondPoly = std::move(second_poly);
}

void MainWindow::recalculateSolvability()
{
	bool first_has_poly = !ui->firstPolynomial->text().isEmpty();
	bool second_has_poly = !ui->secondPolynomial->text().isEmpty();

	emit solvabilityChanged(first_has_poly && second_has_poly);
}

MainWindow::~MainWindow()
{
	delete ui;
}

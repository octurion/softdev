#ifndef GRAPHVISUALIZER_H
#define GRAPHVISUALIZER_H

#include <QDialog>

#include <QtGnuplot/QtGnuplotWidget.h>
#include <QtGnuplot/QtGnuplotInstance.h>

namespace Ui {
class GraphVisualizer;
}

class GraphVisualizer : public QDialog
{
    Q_OBJECT

public:
    explicit GraphVisualizer(QWidget *parent = 0);

	void setPolynomialsAndPoints(std::string firstStr, std::string secondStr,
			std::vector<std::pair<double, double>> vec);

    ~GraphVisualizer();

signals:
	/**
	 * Called every time the graph has changed and needs to be redrawn.
	 */
	void graphChanged();

private slots:
	void redrawGraph();

private:
    Ui::GraphVisualizer *ui;
	QtGnuplotInstance* instance;

	std::string firstPoly;
	std::string secondPoly;

	std::vector<std::pair<double, double>> points;
};

#endif // GRAPHVISUALIZER_H

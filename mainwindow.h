#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "sylvester/eigensolution.hpp"

#include <QMainWindow>

#include <QtGnuplot/QtGnuplotWidget.h>
#include <QtGnuplot/QtGnuplotInstance.h>

#include <vector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void solveSystem();

	void readPolynomialsFromFile();

	/**
	 * Set the minimum, maximum singular value and condition number
	 * for the initial problem.
	 */
	void setMinimumSingularValue(double value);
	void setMaximumSingularValue(double value);
	void setConditionNumber(double value);

	/**
	 * Set the minimum, maximum singular value and condition number
	 * for the variable change problem.
	 */
	void varChangeSetMinimumSingularValue(double value);
	void varChangeSetMaximumSingularValue(double value);
	void varChangeSetConditionNumber(double value);

	/**
	 * Set the solution method for the initial problem.
	 */
	void setSolutionMethod(softdev::SylvesterPolynomialIssue issue);

	/**
	 * Set the solution method for the variable change problem.
	 */
	void varChangeSetSolutionMethod(softdev::SylvesterPolynomialIssue issue);

	/**
	 * Sets the fraction that was used to perform the variable change.
	 */
	void varChangeSetParameters(int t_1, int t_2, int t_3, int t_4);

	/**
	 * Indicates that the variable change failed.
	 */
	void varChangeSetFailed();

	/**
	 * Set the roots for the initial problem, as well as evaluate the
	 * solutions against the polynomials and determining whether or
	 * not they are acceptable.
	 */
	void setInitialRoots(std::vector<std::pair<double, double>> newRoots,
			const softdev::BivariatePolynomial& first,
			const softdev::BivariatePolynomial& second,
			double epsilon);

	/**
	 * Set the roots for the variable change, as well as evaluate the
	 * solutions against the polynomials and determining whether or
	 * not they are acceptable.
	 */
	void setChangedRoots(std::vector<std::pair<double, double>> newRoots,
			const softdev::BivariatePolynomial& first,
			const softdev::BivariatePolynomial& second,
			double epsilon);

	void recalculateSolvability();

signals:
	void solvabilityChanged(bool solvable);
	void solutionExistenceChanged(bool has_solutions);
	void varChangeSolutionExistenceChanged(bool has_solutions);

private:
	Ui::MainWindow* ui;

	softdev::BivariatePolynomial firstPoly;
	softdev::BivariatePolynomial secondPoly;

	std::vector<std::pair<double, double>> initialRoots;
	std::vector<std::pair<double, double>> changedRoots;

	softdev::EigenSolution solution;

	QString issueToString(softdev::SylvesterPolynomialIssue issue) const;
};

#endif // MAINWINDOW_H

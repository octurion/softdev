#pragma once

#include <Eigen/Dense>

#include "binomial.hpp"

#include <complex>
#include <initializer_list>
#include <stdexcept>
#include <vector>

namespace softdev {
	class UnivariatePolynomial;

	/**
	 * Bivariate polynomial whose maximum degree (sum of degree of x and y in
	 * a monomial) is specified during construction.
	 */
	class BivariatePolynomial
	{
		private:
			Eigen::MatrixXd grid;

			/**
			 * Bivariate polynomial constructor with the maximum degree
			 * (sum of degree of x and y per monomial) specified.
			 */
			explicit BivariatePolynomial(size_t x_degree, size_t y_degree)
				: grid(x_degree + 1, y_degree + 1)
			{
				grid.setZero();
			}

		public:
			using Index = Eigen::MatrixXd::Index;

			/**
			 * Constructs a constant polynomial (where the constant is
			 * initially zero).
			 */
			BivariatePolynomial()
				: BivariatePolynomial(0, 0)
			{
			}

			/**
			 * Returns the coefficient of the polynomial that has the degrees
			 * specified.
			 */
			double operator()(Index x_degree, Index y_degree) const
			{
				return grid(x_degree, y_degree);
			}

			/**
			 * Returns a view of the polynomial as a 2-dimensional grid
			 */
			const Eigen::MatrixXd& as_grid() const
			{
				return grid;
			}

			/**
			 * Evaluates the polynomial for a specific value of x and y
			 * (complex number version)
			 */
			std::complex<double> evaluate_for(std::complex<double> x, std::complex<double> y) const
			{
				std::complex<double> result = 0;

				for (Index j = 0; j < grid.cols(); j++) {
					for (Index i = 0; i < grid.rows(); i++) {
						result += grid(i, j) * pow(x, i) * pow(y, j);
					}
				}

				return result;
			}

			/**
			 * Evaluates the polynomial for a specific value of x and y
			 */
			double evaluate_for(double x, double y) const
			{
				double result = 0;

				for (Index j = 0; j < grid.cols(); j++) {
					for (Index i = 0; i < grid.rows(); i++) {
						result += grid(i, j) * pow(x, i) * pow(y, j);
					}
				}

				return result;
			}

			/**
			 * Evaluates the polynomial for a specific value of x
			 */
			UnivariatePolynomial evaluate_for_x(double x) const;

			/**
			 * Evaluates the polynomial for a specific value of y
			 */
			UnivariatePolynomial evaluate_for_y(double y) const;

			Index x_rank() const
			{
				return grid.rows() - 1;
			}

			Index y_rank() const
			{
				return grid.cols() - 1;
			}

			/**
			 * Returns a string representation for this polynomial. The return
			 * value of this function will be always successfully parsed from
			 * `BivariatePolynomial::parse()`.
			 */
			std::string to_string() const;

			/**
			 * Returns a string representation for this polynomial, suitable
			 * for use by Gnuplot.
			 */
			std::string to_gnuplot_string() const;

			/**
			 * Parses a polynomial from a string until the first '\0' or '\n'.
			 *
			 * The parser supports the following features:
			 * - Whitespace is allowed everywhere
			 * - You can omit the multiplication symbol. `2*x^2`, `2 x^2` and
			 *   `2x^2` are all treated equally.
			 * - You can swap the locations of x and y. `x^2 y^2` and `y^2 x^2`
			 *   are treated equally.
			 * - You can omit an exponent.
			 * - You can omit x and/or y entirely.
			 * - You can omit the constant (but, of course, you must specify
			 *   an x and/or y).
			 * - `x^2 + 2x^2` and `3x^2` will be treated equally.
			 * - The monomials don't have to be specified in any particular
			 *   order.
			 * - A leading sign is allowed.
			 * - Decimal and exponential form is allowed.
			 *
			 * Parameters requiring clarification:
			 * - degree: The maximum degree of the polynomial (the function
			 *   will throw in case of a larger degree)
			 */
			static BivariatePolynomial parse(const char* string);

			/**
			 * Construct an interpolated polynomial with the specified degree
			 * from the given set of points.
			 */
			static BivariatePolynomial from_interpolation(size_t degree,
					const std::vector<std::pair<double, double>>& points);

	};

	/**
	 * Construct an interpolation matrix with the specified degree from
	 * the given set of points.
	 *
	 * This function is public, so that we can perform unit tests on it.
	 */
	Eigen::MatrixXd interpolation_matrix(size_t degree,
			const std::vector<std::pair<double, double>>& points);

	/**
	 * Univariate polynomial whose maximum degree is specified during
	 * construction.
	 */
	class UnivariatePolynomial
	{
		private:
			std::vector<double> coefficients;

		public:
			using iterator = std::vector<double>::iterator;
			using const_iterator = std::vector<double>::const_iterator;

			/**
			 * Univariate constant polynomial constructor with zero coefficient.
			 */
			UnivariatePolynomial()
				: UnivariatePolynomial(0)
			{
			}

			explicit UnivariatePolynomial(std::vector<double> coefficients)
				: coefficients(std::move(coefficients))
			{
			}

			template<typename Iter>
			UnivariatePolynomial(Iter begin, Iter end)
				: coefficients(begin, end)
			{
			}

			/**
			 * Univariate polynomial constructor with the degree specified.
			 */
			explicit UnivariatePolynomial(size_t poly_degree)
				: coefficients(poly_degree + 1, 0)
			{
				if (poly_degree == std::numeric_limits<size_t>::max()) {
					throw std::invalid_argument("Degree passed is too big");
				}
			}

			UnivariatePolynomial(const std::initializer_list<double>& list)
				: coefficients(list)
			{
			}

			/**
			 * Returns the maximum degree of this polynomial.
			 */
			size_t degree() const { return coefficients.size() - 1; }

			/**
			 * Returns the nth degree of this polynomial or zero if out of
			 * bounds.
			 */
			double at(size_t idx) const {
				if (idx >= coefficients.size()) {
					return 0;
				}

				return coefficients[idx];
			}

			/**
			 * Returns the nth degree of this polynomial (bounds-checking
			 * version).
			 */
			double& operator[](size_t idx) {
				return coefficients[idx];
			}

			/**
			 * Returns the nth degree of this polynomial (bounds-checking
			 * const version).
			 */
			const double& operator[](size_t idx) const {
				return coefficients[idx];
			}

			iterator begin()
			{
				return coefficients.begin();
			}

			iterator end()
			{
				return coefficients.end();
			}

			const_iterator begin() const
			{
				return coefficients.begin();
			}

			const_iterator end() const
			{
				return coefficients.end();
			}

			const_iterator cbegin() const
			{
				return coefficients.cbegin();
			}

			const_iterator cend() const
			{
				return coefficients.cend();
			}

			double evaluate_for(double x) const
			{
				double result = 0;
				double x_raised = 1;
				for (const auto& e : coefficients) {
					result += x_raised * e;
					x_raised *= x;
				}

				return result;
			}

			UnivariatePolynomial operator*(const UnivariatePolynomial& rhs) const
			{
				size_t new_degree = degree() + rhs.degree();
				UnivariatePolynomial result(new_degree);

				for (size_t i = 0; i <= degree(); i++) {
					for (size_t j = 0; j <= rhs.degree(); j++) {
						result[i + j] += (*this)[i] * rhs[j];
					}
				}

				return result;
			}

			UnivariatePolynomial& operator*=(const UnivariatePolynomial& rhs)
			{
				auto result = rhs * (*this);

				*this = std::move(result);
				return *this;
			}

			/**
			 * Constructs a polynomial of the form (ax + b)^n
			 */
			static UnivariatePolynomial first_degree_poly_pow(
					int x_coeff, int const_coeff, unsigned pow,
					const BinomialCoefficients& precomputed);

			/**
			 * Returns the real roots of this polynomial, if any.
			 */
			std::vector<double> real_roots() const;
	};
}

#pragma once

#include "grid.hpp"

#include <cstdint>

namespace softdev
{
	/**
	 * A precomputed table of all values of "n choose k" up to the value of `n`
	 * specified. Necessary in order to speed up computations involving
	 * binomial coefficients, etc.
	 */
	class BinomialCoefficients
	{
		private:
			Grid<uint64_t> values;

			explicit BinomialCoefficients(size_t num)
				: values(num + 1)
			{
			}

		public:
			/**
			 * Constructs a binomial coefficient for all "n choose k" binomial
			 * coefficients up to `n`.
			 */
			static BinomialCoefficients up_to(size_t n);

			/**
			 * Returns the maximum value of `n`.
			 */
			size_t num() const { return values.dimension() - 1; };

			/**
			 * Retrieve the value of "n choose k". Throws if out of bounds.
			 */
			uint64_t operator()(size_t n, size_t k) const
			{
				if (n > num() || k > n) {
					throw std::invalid_argument("n choose k out of bounds");
				}

				return values(n, k);
			}
	};
}

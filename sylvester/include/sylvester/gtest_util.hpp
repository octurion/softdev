#pragma once

#define EIGEN_INITIALIZE_MATRICES_BY_NAN

/**
 * Utility stuff to use with Google Test.
 */

#define EIGEN_MATRIXBASE_PLUGIN "eigen_gtest_addons.hpp"
#include <Eigen/Dense>
#include <gmock/gmock.h>

MATCHER_P2(MatrixApproximateTo, expect, epsilon,
		std::string(negation ? "isn't" : "is")
			+ " approximate to " + testing::PrintToString(expect)
			+ "\nwhere the epsilon value is " + testing::PrintToString(epsilon))
{
	return arg.rows() == expect.rows()
		&& arg.cols() == expect.cols()
		&& arg.isApprox(expect, epsilon);
}

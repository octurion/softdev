#pragma once

#include <Eigen/Dense>

#include "polynomial.hpp"

#include <memory>
#include <vector>

namespace softdev
{
	enum Variable { X, Y };

	/**
	 * An n-by-n implementation of a Sylvester matrix holding polynomials.
	 * Because a Sylvester matrix tends to have lots of zeros, as well as
	 * multiple copies of polynomials, we reduce the amount of memory wasted
	 * by using shared pointers (that implement reference counting).
	 */
	class SylvesterMatrix
	{
		private:
			Variable hidden_var;
			size_t hidden_var_degree;

			// We want to use shared pointers here to minimize the amount
			// of memory that we would waste if we used copy constructors, etc.
			Grid<std::shared_ptr<UnivariatePolynomial>> s;

			void sylvester_fill(const BivariatePolynomial& first, const BivariatePolynomial& second);

		public:
			/**
			 * Not useful. Only provided so as to construct a default dummy
			 * matrix.
			 */
			SylvesterMatrix()
				: hidden_var(Variable::Y)
				, hidden_var_degree(0)
				, s()
			{ }

			/**
			 * Constructs a Sylvester matrix with the specified hidden variable,
			 * dimension (of the square matrix) and the degree of the hidden
			 * variable.
			 */
			explicit SylvesterMatrix(size_t dimension, Variable hidden_var, size_t hidden_var_degree)
				: hidden_var(hidden_var)
				, hidden_var_degree(hidden_var_degree)
				, s(dimension)
			{
				// Because creating n x n instances of a zero polynomial is wasteful,
				// we'll just create a shared reference to a zero polynomial
				// and fill the entire grid
				std::fill(s.begin(), s.end(), std::make_shared<UnivariatePolynomial>());
			}

			/**
			 * Returns the degree of this matrix.
			 */
			size_t degree() const { return s.dimension(); };

			/**
			 * Returns the hidden variable of this Sylvester matrix.
			 */
			Variable hidden_variable() const { return hidden_var; }

			/**
			 * Returns the hidden variable degree of this Sylvester matrix.
			 * (Useful when creating the Sylvester polynomial).
			 */
			size_t hidden_variable_degree() const { return hidden_var_degree; }

			/**
			 * Returns the polynomial stored in row i and column j
			 * (both zero-indexed). Bounds checking is performed.
			 */
			const UnivariatePolynomial& operator() (size_t i, size_t j) const
			{
				return *s(i, j);
			}

			/**
			 * Construct a Sylvester matrix from two bivariate polynomials
			 * specified. Throws if the degrees of the polynomials are
			 * not suitable enough to construct a Sylvester matrix.
			 */
			static SylvesterMatrix from_polynomials(
					const BivariatePolynomial& first,
					const BivariatePolynomial& second);
	};

	/**
	 * An implementation of a Sylvester polynomial (aka a polynomial where
	 * its coefficients are square matrices and is constructed from its
	 * equivalent Sylvester matrix).
	 */
	class SylvesterPolynomial
	{
		private:
			std::vector<Eigen::MatrixXd> matrix_coeffs;
			Variable hidden_var;

		public:
			/**
			 * Construct a Sylvester polynomial with the degree and dimension
			 * specified.
			 */
			SylvesterPolynomial(size_t poly_degree, size_t matrix_dimension,
					Variable hidden_var)
				: matrix_coeffs(poly_degree + 1,
						Eigen::MatrixXd(matrix_dimension, matrix_dimension))
				, hidden_var(hidden_var)
			{
				for (auto& mat: matrix_coeffs) {
					mat.setZero();
				}
			}

			SylvesterPolynomial(const std::initializer_list<Eigen::MatrixXd>& list)
				: matrix_coeffs(list)
				, hidden_var(Variable::Y)
			{
			}

			Variable hidden_variable() const
			{
				return hidden_var;
			}

			/**
			 * Returns the degree of this Sylvester polynomial.
			 */
			size_t degree() const
			{
				return matrix_coeffs.size() - 1;
			}

			/**
			 * Returns the dimension (matrix rows & cols) of this Sylvester polynomial.
			 */
			size_t dimension() const
			{
				return matrix_coeffs[0].rows();
			}

			const Eigen::MatrixXd& operator[](size_t i) const
			{
				return matrix_coeffs[i];
			}

			Eigen::MatrixXd& operator[](size_t i)
			{
				return matrix_coeffs[i];
			}

			const Eigen::MatrixXd& max_degree_mat() const
			{
				return matrix_coeffs.back();
			}

			/**
			 * Evaluates this polynomial for a specific value.
			 */
			Eigen::MatrixXd evaluate_for(double value)
			{
				if (matrix_coeffs.empty()) {
					Eigen::MatrixXd mat(dimension(), dimension());
					mat.setZero();

					return mat;
				}

				Eigen::MatrixXd mat = matrix_coeffs.front();
				auto value_exp = value;
				for (size_t i = 1; i <= degree(); i++) {
					mat += matrix_coeffs[i] * double(value_exp);
					value_exp *= value;
				}

				return mat;
			}

			/**
			 * Constructs a Sylvester polynomial from a Sylvester matrix.
			 */
			static SylvesterPolynomial from_sylvester_matrix(const SylvesterMatrix& matrix);

			/**
			 * Performs the variable change `y = \frac{t_1 z + t_2}{t_3 z + t_4}`
			 * on this Sylvester polynomial. The Sylvester polynomial returned
			 * is not the actual one (with fractions instead of y), but
			 * one whose eigensolutions are the same compared to the proper
			 * polynomial.
			 */
			SylvesterPolynomial var_change(int t_1, int t_2, int t_3, int t_4);
	};
}

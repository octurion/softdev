#pragma once

#include <stdexcept>
#include <vector>

namespace softdev
{
	/*
	 * Row-major square grid whose dimension is specified at construction and
	 * cannot be changed.
	 */
	template <typename T>
	class Grid
	{
		private:
			size_t grid_dimension;
			std::vector<T> grid;

			size_t index(size_t row, size_t col) const
			{
				if (grid_dimension == 0 || row > grid_dimension || col > grid_dimension) {
					throw std::out_of_range("Row and/or column is out of range");
				}

				return row * grid_dimension + col;
			}

		public:
			typedef typename std::vector<T>::iterator iterator;
			typedef typename std::vector<T>::const_iterator const_iterator;

			/**
			 * Default constructor that constructs a zero by zero grid and
			 * allocates nothing.
			 */
			Grid()
				: grid_dimension(0)
				, grid()
			{ }

			/**
			 * Constructor that sets the grid's dimension and default-initializes
			 * each grid element. Throws ```std::invalid_argument()``` if the
			 * grid size would cause an integer overflow.
			 */
			explicit Grid(size_t dimension)
				: grid_dimension(dimension)
				, grid(dimension * dimension)
			{
				if (dimension > grid.max_size() / dimension) {
					throw std::invalid_argument("Dimension passed is too big");
				}
			}

			size_t dimension() const
			{
				return grid_dimension;
			}

			/**
			 * Returns the number of elements in the grid
			 */
			size_t size() const
			{
				return grid.size();
			}

			/**
			 * Grid indexing operation.
			 *
			 * We can't use the array indexing operator and the comma operator
			 * together, so we'll just stick to using parentheses for indexing.
			 */
			T& operator() (size_t row, size_t col)
			{
				return grid[index(row, col)];
			}

			/**
			 * Const version of the grid indexing operation.
			 */
			const T& operator() (size_t row, size_t col) const
			{
				return grid[index(row, col)];
			}

			/**
			 * Row-major iterator that points to the top-left element.
			 */
			iterator begin()
			{
				return grid.begin();
			}

			/**
			 * Row-major iterator that points to one past after the bottom
			 * right element.
			 */
			iterator end()
			{
				return grid.end();
			}

			const_iterator begin() const
			{
				return grid.begin();
			}

			const_iterator end() const
			{
				return grid.end();
			}

			const_iterator cbegin() const
			{
				return grid.cbegin();
			}

			const_iterator cend() const
			{
				return grid.cend();
			}

			/**
			 * Row-major iterator that points to the element specified.
			 */
			iterator begin_from(size_t row, size_t col)
			{
				auto idx = index(row, col);
				return grid.begin() + idx;
			}

			const_iterator begin_from(size_t row, size_t col) const
			{
				auto idx = index(row, col);
				return grid.begin() + idx;
			}

			/**
			 * Row-major iterator that points to one past after the element
			 * specified (which makes it useful for functions found in
			 * ```std::algorithm``` etc).
			 */
			iterator end_upto(size_t row, size_t col)
			{
				auto idx = index(row, col);
				return grid.begin() + idx + 1;
			}

			const_iterator end_upto(size_t row, size_t col) const
			{
				auto idx = index(row, col);
				return grid.begin() + idx + 1;
			}

			/**
			 * Const version of ```begin_from()```.
			 */
			const_iterator cbegin_from(size_t row, size_t col) const
			{
				auto idx = index(row, col);
				return grid.begin() + idx;
			}

			/**
			 * Const version of ```end_upto()```.
			 */
			const_iterator cend_upto(size_t row, size_t col) const
			{
				auto idx = index(row, col);
				return grid.begin() + idx + 1;
			}
	};
}

#pragma once

#include <Eigen/Dense>

#include "sylvester.hpp"

#include <utility>
#include <vector>

namespace softdev
{
	typedef std::pair<std::complex<double>,
			std::vector<std::complex<double>>> XValues;

	enum class SylvesterPolynomialIssue
	{
		None, KappaTooBig, MinCoeffTooSmall
	};

	/**
	 * Returned from `sylvester_polynomial_info()`. Stores the metadata
	 * necessary to solve the eigenvalues/eigenvectors with the correct
	 * methodology.
	 */
	class CoefficientInfo
	{
		private:
			double min_coeff;
			double max_coeff;

			double kappa_value;

		public:
			CoefficientInfo()
				: min_coeff(0)
				, max_coeff(0)
				, kappa_value(0)
			{
			}

			CoefficientInfo(double min_coeff, double max_coeff)
				: min_coeff(min_coeff)
				, max_coeff(max_coeff)
				, kappa_value(max_coeff / min_coeff)
			{
			}

			double min_coefficient() const
			{
				return min_coeff;
			}

			double max_coefficient() const
			{
				return max_coeff;
			}

			double kappa() const
			{
				return kappa_value;
			}
	};

	/**
	 * Returned from `sylvester_polynomial_eigensolution()`. Stores the
	 * eigenvalues/eigenvectors found while solving either the standard
	 * or the generalized eigenproblem.
	 *
	 * The reason this type is a struct with all members public is because
	 * we had an issue with creating a constructor for this class (Eigen
	 * always failed with an assertion for some reason).
	 */
	struct EigenSolution
	{
		Eigen::VectorXcd eigenvalues;
		Eigen::MatrixXcd eigenvectors;

		SylvesterPolynomialIssue issue;
	};


	/**
	 * List of all real eigenvalues. Each eigenvalue also contains all the
	 * real solutions (may also contain Inf and NaN values) for its
	 * corresponding eigenvector.
	 */
	class Roots
	{
		public:
			typedef std::pair<double, std::vector<double>> EigenRoots;

		private:
			std::vector<EigenRoots> roots;

			/**
			 * Insert an eigenvalue/eigenroot pair by taking epsilon proximity
			 * into account.
			 */
			void insert(double eigenvalue, double eigenroot,
					double eigenvalue_closeness_epsilon);

		public:
			Roots()
			{
			}

			explicit Roots(std::vector<EigenRoots> roots)
				: roots(roots)
			{
			}

			/**
			 * Constructs a list of all eigenvalues from the solutions of
			 * an eigenproblem. For each eigenvalue, the corresponding roots
			 * from its corresponding eigenvectors are stored, so that
			 * multiplicity is tracked.
			 *
			 * Proximity when storing eigenvalues is taken into account. Also,
			 * eigenvalues and eigenvalue roots may be Inf and/or NaN.
			 */
			static Roots from_eigenvectors(const EigenSolution& solutions,
					double eigenvalue_closeness_epsilon);

			/**
			 * Returns all the (x, y) pairs that solve the system. This
			 * function is hidden variable-aware.
			 */
			std::vector<std::pair<double, double>> root_pairs(
					const BivariatePolynomial& first,
					const BivariatePolynomial& second,
					const SylvesterPolynomial& poly,
					double epsilon) const;
	};

	class YValues
	{
		private:
			std::vector<XValues> y_solutions;

		public:
			YValues(std::vector<XValues> y_solutions)
				: y_solutions(std::move(y_solutions))
			{
			}

			/**
			 * Constructs a list of all complex y values from the solutions of
			 * an eigenproblem. For each y value, its corresponding x values
			 * are stored, so that multiplicity is tracked.
			 *
			 * Proximity when storing y values is taken into account. Also,
			 * x and/or y may have infinities and NaNs in their real and/or
			 * imaginary parts.
			 */
			static YValues from_eigenvectors(const EigenSolution& solution);

			/**
			 * Prints all acceptable solutions corresponding to the two
			 * polynomials. Printing is done as follows:
			 *
			 * - Any y value with a non-finite real/imaginary part is skipped.
			 * - For each y value with a multiplicity of at least 2,
			 *   only the multiplicity is printed.
			 * - Otherwise, the pair of x and y is printed, as well as its
			 *   how close this solution is to zero for both polynomials. The
			 *   solution is deemed acceptable or not depending on the value
			 *   of epsilon (if the magnitude of the solution is at most
			 *   epsilon, it is considered acceptable).
			 */
			void print_acceptable_solutions(FILE* out,
					const BivariatePolynomial& first,
					const BivariatePolynomial& second,
					double epsilon) const;
	};

	/**
	 * Returns the metadata necessary to solve the eigenvalues/eigenvectors
	 * with the correct methodology. The metadata stored are the smallest &
	 * largest singular value, as well as their ratio (named Kappa).
	 */
	CoefficientInfo sylvester_polynomial_info(
			const SylvesterPolynomial& polynomial);

	/**
	 * Solves the complex eigenvalues/eigenvectors problem given a Sylvester
	 * polynomial, its already precomputed metadata regarding its coefficients
	 * and the bound variable B.
	 *
	 * The eigenvalues/eigenvectors returned have the following property:
	 * If the i-th one is a complex eigenvalue/eigenvector, then the (i+1)-th
	 * one is its complex conjugate. This property holds thanks to the way
	 * LAPACK and Eigen solve the standard and the generalized eigenproblem.
	 *
	 * This function does not return all of the eigenvectors' rows, but only
	 * the first `polynomial.dimension()` ones, because the rest of them are
	 * unnecessary.
	 *
	 * In addition, this function returns which methodology was used to solve
	 * the eigenproblem, as well as the reason why by using values of the
	 * `SylvesterPolynomialIssue` enum.
	 */
	EigenSolution sylvester_polynomial_eigensolution(
			const SylvesterPolynomial& polynomial,
			const CoefficientInfo& info,
			double bound_exp);

	/**
	 * Modifies the eigenvalues of an eigensolution so that the eigenvalues
	 * correspond from the variable z to the variable y.
	 */
	EigenSolution revert_variable_change(EigenSolution solution,
			int t_1, int t_2, int t_3, int t_4);

	/**
	 * Constructs the companion matrix of the polynomial specified.
	 */
	Eigen::MatrixXd companion_matrix_of(const SylvesterPolynomial& polynomial);

	/**
	 * Constructs the matrices for the generalized eigenproblem. First
	 * matrix corresponds to the first degree. Second one corresponds to the
	 * constant degree.
	 */
	std::tuple<Eigen::MatrixXd, Eigen::MatrixXd> generalized_eigenproblem_matrices_of(
			const SylvesterPolynomial& polynomial);
}

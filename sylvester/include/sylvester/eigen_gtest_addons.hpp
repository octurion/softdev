// Allows us to print matrices on Google Test. Don't include this directly.
// Instead, use the `gtest_util.hpp` header.
friend void PrintTo(const Derived &m, ::std::ostream *o) {
    *o << "\n" << m;
}

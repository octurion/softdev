#include "binomial.hpp"

namespace softdev
{
	BinomialCoefficients BinomialCoefficients::up_to(size_t n)
	{
		BinomialCoefficients result(n);

		result.values(0, 0) = 1;

		for (size_t i = 1; i <= n; i++) {
			result.values(i, 0) = 1;
			for (size_t j = 1; j < i; j++) {
				result.values(i, j) =
					result.values(i - 1, j - 1) + result.values(i - 1, j);
			}
			result.values(i, i) = 1;
		}

		return result;
	}
}

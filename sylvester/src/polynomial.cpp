#include "polynomial.hpp"

#include <cctype>
#include <cerrno>
#include <cinttypes>
#include <cmath>
#include <functional>
#include <numeric>
#include <stdexcept>
#include <string>
#include <sstream>
#include <unordered_map>
#include <utility>
#include <vector>

#include <Eigen/Dense>

namespace
{
	int intpow(int a, unsigned pow)
	{
		int result = 1;
		int temp = a;

		for (; pow != 0; pow >>= 1) {
			if ((pow & 1) != 0) {
				result *= temp;
			}
			temp *= temp;
		}

		return result;
	}

	struct PairHash
	{
		size_t operator()(const std::pair<size_t, size_t>& key) const {
			auto first = std::hash<size_t>()(key.first);
			auto second = std::hash<size_t>()(key.second);

			return 31 * first + second;
		}
	};
}

namespace softdev {
	using Eigen::RowVectorXd;
	using Eigen::MatrixXd;
	using Eigen::VectorXcd;
	using Eigen::VectorXd;

	using std::random_device;
	using std::mt19937;
	using std::uniform_int_distribution;

	using std::placeholders::_1;

	using std::isfinite;
	using std::make_pair;
	using std::unordered_map;
	using std::pair;
	using std::string;
	using std::stringstream;
	using std::vector;

	void skip_whitespace(const char** ptr)
	{
		while (**ptr == ' ') {
			(*ptr)++;
		}
	}

	bool matches_char(const char** ptr, char ch)
	{
		skip_whitespace(ptr);

		return **ptr == ch;
	}

	/**
	 * Parses a monomial expression like `x^420`, `y`, `y^0`, `x^1` as well
	 * as a (potentially nonexistent) leading multiplication symbol.
	 *
	 * Returns the exponent of the monomial.
	 */
	intmax_t parse_monomial_exp(const char** ptr, char var_name)
	{
		// We accept both the existence and the absence of a multiplication
		// symbol
		if (matches_char(ptr, '*')) {
			(*ptr)++;
		}

		// Constant case
		if (!matches_char(ptr, var_name))
		{
			return 0;
		}
		(*ptr)++;

		// Variable name without exponent
		if (!matches_char(ptr, '^'))
		{
			return 1;
		}
		(*ptr)++;

		// We have to skip whitespace, or `strtoimax()` may return nonsense,
		// because it attempts to skip leading whitespace.
		skip_whitespace(ptr);

		char* endptr;
		errno = 0;
		intmax_t exp = strtoimax(*ptr, &endptr, 10);
		if (errno != 0) {
			throw std::invalid_argument("Unsuccessful integer parse");
		}

		*ptr = endptr;
		return exp;
	}

	BivariatePolynomial BivariatePolynomial::parse(const char* string)
	{
		unordered_map<pair<size_t, size_t>, double, PairHash> map;

		if (string == nullptr) {
			throw std::invalid_argument("String passed cannot be null");
		}

		const char* cursor = string;
		skip_whitespace(&cursor);

		if (*cursor == '\0' || *cursor == '\n') {
			throw std::invalid_argument("Empty strings are not allowed");
		}

		// Leading sign case
		bool must_negate = false;
		if (*cursor == '+' || *cursor == '-') {
			must_negate = (*cursor == '-');
			cursor++;
		}

		for (;;) {
			skip_whitespace(&cursor);

			// Default value in case of expressions like `x^2`
			double coeff = 1;
			if (isdigit(*cursor)) {
				char* endptr;
				errno = 0;
				coeff = strtod(cursor, &endptr);
				if (errno != 0) {
					throw std::invalid_argument("Unsuccessful integer parse");
				}
				cursor = endptr;
			}

			long x_degree;
			long y_degree;

			if (matches_char(&cursor, '*')) {
				cursor++;
			}

			// Accept even if y variable shows up before x variable
			if (matches_char(&cursor, 'x')) {
				x_degree = parse_monomial_exp(&cursor, 'x');
				y_degree = parse_monomial_exp(&cursor, 'y');
			} else {
				y_degree = parse_monomial_exp(&cursor, 'y');
				x_degree = parse_monomial_exp(&cursor, 'x');
			}

			// Make out of bounds degree check explicit to the user
			if (x_degree < 0
					|| size_t(x_degree) >= std::numeric_limits<uintmax_t>::max()) {
				throw std::invalid_argument("X exponent too large");
			}
			if (y_degree < 0
					|| size_t(y_degree) >= std::numeric_limits<uintmax_t>::max()) {
				throw std::invalid_argument("Y exponent too large");
			}

			if (must_negate) {
				coeff = -coeff;
			}
			map[{x_degree, y_degree}] += coeff;

			skip_whitespace(&cursor);
			if (*cursor == '\0' || *cursor == '\n') {
				break;
			}

			// If the polynomial doesn't terminate, we expect a term and this
			// term must be followed by a plus or minus sign.
			if (*cursor != '+' && *cursor != '-') {
				throw std::invalid_argument("Expected sign");
			}

			must_negate = (*cursor == '-');
			cursor++;
			skip_whitespace(&cursor);
		}

		auto fold_fn = [](
				const pair<size_t, size_t>& lhs,
				const pair<pair<size_t, size_t>, double>& rhs) -> pair<size_t, size_t>
		{
			return make_pair(
					std::max(lhs.first, rhs.first.first),
					std::max(lhs.second, rhs.first.second));
		};

		auto degrees = std::accumulate(map.cbegin(), map.cend(), make_pair(0, 0), fold_fn);

		BivariatePolynomial result(degrees.first, degrees.second);
		for (const auto& kv : map) {
			result.grid(kv.first.first, kv.first.second) = kv.second;
		}

		return result;
	}

	UnivariatePolynomial UnivariatePolynomial::first_degree_poly_pow(
			int x_coeff, int const_coeff, unsigned pow,
			const BinomialCoefficients& precomputed)
	{
		if (pow > precomputed.num()) {
			throw std::invalid_argument("Binomial precomputed table too small");
		}

		if (pow == 0) {
			return {1};
		}

		UnivariatePolynomial poly(pow);

		if (const_coeff == 0) {
			poly[pow] = intpow(x_coeff, pow);

			return poly;
		}

		int a = 1;
		int b = intpow(const_coeff, pow);

		for (size_t i = 0; i <= pow; i++) {
			poly[i] = precomputed(pow, i) * a * b;

			a *= x_coeff;
			b /= const_coeff;
		}

		return poly;
	}

	string BivariatePolynomial::to_string() const
	{
		if (grid.rows() == 0 || grid.cols() == 0) {
			return "";
		}

		stringstream stream;
		for (Index i = 0; i < grid.rows(); i++) {
			for (Index j = 0; j < grid.cols(); j++) {
				if (i == 0 && j == 0) {
					stream << grid(i, j);
					continue;
				}

				if (std::abs(grid(i, j)) < std::numeric_limits<float>::epsilon()) {
					continue;
				}

				stream << (grid(i, j) < 0 ? '-' : '+') << std::abs(grid(i, j));
				if (i > 0) {
					stream << "x";
					if (i > 1) {
						stream << "^" << i;
					}
				}

				if (j > 0) {
					stream << "y";
					if (j > 1) {
						stream << "^" << j;
					}
				}
			}
		}

		return stream.str();
	}

	string BivariatePolynomial::to_gnuplot_string() const
	{
		if (grid.rows() == 0 || grid.cols() == 0) {
			return "";
		}

		stringstream stream;
		for (Index i = 0; i < grid.rows(); i++) {
			for (Index j = 0; j < grid.cols(); j++) {
				if (i == 0 && j == 0) {
					stream << grid(i, j);
					continue;
				}

				if (std::abs(grid(i, j)) < std::numeric_limits<float>::epsilon()) {
					continue;
				}

				stream << (grid(i, j) < 0 ? '-' : '+') << std::abs(grid(i, j));
				if (i > 0) {
					stream << "*x";
					if (i > 1) {
						stream << "**" << i;
					}
				}

				if (j > 0) {
					stream << "*y";
					if (j > 1) {
						stream << "**" << j;
					}
				}
			}
		}

		return stream.str();
	}

	vector<double> UnivariatePolynomial::real_roots() const
	{
		auto it = std::find_if_not(coefficients.rbegin(), coefficients.rend(),
			std::bind(std::equal_to<double>(), 0, _1)).base();

		auto count = std::distance(coefficients.begin(), it);
		if (count == 0) {
			throw std::logic_error("Zero polynomial has infinite roots");
		}

		if (count == 1) {
			return vector<double>();
		}

		MatrixXd matrix(count - 1, count - 1);
		matrix.setZero();
		matrix.topRightCorner(count - 2, count - 2).diagonal().setOnes();

		auto max_degree_coeff = coefficients[count - 1];
		for (decltype(count) i = 0; i < count - 1; i++) {
			matrix.bottomRows<1>()[i] = - coefficients[i] / max_degree_coeff;
		}

		auto eigenvalues = matrix.eigenvalues();

		vector<double> results;
		for (VectorXcd::Index i = 0; i < eigenvalues.size(); i++) {
			auto real = eigenvalues[i].real();
			auto imag = eigenvalues[i].imag();
			if (!isfinite(real) || !isfinite(imag) || imag != 0) {
				continue;
			}

			results.push_back(real);
		}

		return results;
	}

	UnivariatePolynomial BivariatePolynomial::evaluate_for_x(double x) const
	{
		RowVectorXd x_vec(grid.rows());
		x_vec[0] = 1;
		for (Index i = 1; i < x_vec.size(); i++) {
			x_vec[i] = x * x_vec[i - 1];
		}

		RowVectorXd result = x_vec * grid;

		UnivariatePolynomial poly(y_rank());
		for (Index i = 0; i <= y_rank(); i++) {
			poly[i] = result[i];
		}

		return poly;
	}

	UnivariatePolynomial BivariatePolynomial::evaluate_for_y(double y) const
	{
		VectorXd y_vec(grid.cols());
		y_vec[0] = 1;
		for (Index i = 1; i < y_vec.size(); i++) {
			y_vec[i] = y * y_vec[i - 1];
		}

		VectorXd result = grid * y_vec;

		UnivariatePolynomial poly(x_rank());
		for (Index i = 0; i <= x_rank(); i++) {
			poly[i] = result[i];
		}

		return poly;
	}

	BivariatePolynomial BivariatePolynomial::from_interpolation(size_t degree,
			const std::vector<std::pair<double, double>>& points)
	{
		auto mat = interpolation_matrix(degree, points);

		// Eigen probably has a memory-related bug here. Refactor this
		// and valgrind will complain.
		auto decomposition = mat.fullPivLu();
		auto kernel_computation = decomposition.kernel();

		auto rank_wanted = (degree + 1) * (degree + 2) / 2 - 1;

		if ((uintmax_t) kernel_computation.rank() != rank_wanted) {
			throw std::logic_error("Couldn't create polynomial from interpolation");
		}

		VectorXd kernel = kernel_computation;
		for (VectorXd::Index i = kernel.rows(); i --> 0;) {
			if (fabs(kernel[i]) >= std::numeric_limits<float>::epsilon()) {
				kernel /= kernel[i];
				break;
			}
		}

		BivariatePolynomial result(degree, degree);

		size_t x_max_deg = 0;
		size_t y_max_deg = 0;

		size_t row = 0;
		for (size_t i = 0; i <= degree; i++) {
			for (size_t j = 0; j <= i; j++, row++) {
				auto x_deg = i - j;
				auto y_deg = j;

				if (fabs(kernel[row]) < std::numeric_limits<float>::epsilon()) {
					continue;
				}

				result.grid(x_deg, y_deg) = kernel[row];

				x_max_deg = std::max(x_deg, x_max_deg);
				y_max_deg = std::max(y_deg, y_max_deg);
			}
		}

		if (x_max_deg != degree || y_max_deg != degree) {
			result.grid = result.grid
				.topLeftCorner(x_max_deg + 1, y_max_deg + 1)
				.eval();
		}

		return result;
	}

	MatrixXd interpolation_matrix(size_t degree,
			const std::vector<std::pair<double, double>>& points)
	{
		if (degree == 0) {
			throw std::logic_error("Zero degree not supported");
		}

		if (points.empty()) {
			throw std::logic_error("You must specify at least 1 point");
		}

		auto columns = (degree + 1) * (degree + 2) / 2;

		MatrixXd mat(points.size(), columns);
		mat.setZero();

		mat.leftCols<1>().setOnes();

		size_t col = 1;
		for (size_t j = 1; j <= degree; j++) {
			for (size_t k = 0; k <= j; k++, col++) {
				auto x_deg = j - k;
				auto y_deg = k;

				for (auto it = points.begin(); it != points.end(); it++) {
					auto x = it->first;
					auto y = it->second;

					auto value = std::pow(x, x_deg) * std::pow(y, y_deg);

					auto row = std::distance(points.begin(), it);

					mat(row, col) = value;
				}
			}
		}

		return mat;
	}
}

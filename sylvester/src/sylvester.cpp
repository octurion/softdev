#include "sylvester.hpp"

#include "binomial.hpp"

#include <algorithm>

namespace softdev
{
	using std::get;
	using std::make_shared;

	SylvesterMatrix SylvesterMatrix::from_polynomials(
			const BivariatePolynomial& first,
			const BivariatePolynomial& second)
	{
		auto deg_x = std::max(first.x_rank(), second.x_rank());
		auto deg_y = std::max(first.y_rank(), second.y_rank());

		// In case of equal variable degrees pick Y as the variable to hide
		auto hidden_var = std::min(deg_x, deg_y) == deg_y
			? Variable::Y
			: Variable::X;

		if (hidden_var == Variable::Y
				&& (first.x_rank() == 0 || second.x_rank() == 0)) {
			throw std::invalid_argument(
					"Non-hidden variable X must have non-zero degrees in both polynomials");
		}

		if (hidden_var == Variable::X
				&& (first.y_rank() == 0 || second.y_rank() == 0)) {
			throw std::invalid_argument(
					"Non-hidden variable Y must have non-zero degrees in both polynomials");
		}

		auto matrix_dimension = (hidden_var == X)
			? first.y_rank() + second.y_rank()
			: first.x_rank() + second.x_rank();

		auto hidden_degree = (hidden_var == X)
			? deg_x
			: deg_y;

		SylvesterMatrix matrix(matrix_dimension, hidden_var, hidden_degree);

		matrix.sylvester_fill(first, second);
		return matrix;
	}

	void SylvesterMatrix::sylvester_fill(const BivariatePolynomial& first, const BivariatePolynomial& second)
	{
		// Non-hidden variable degree (first polynomial)
		auto d0 = (hidden_var == Variable::X)
			? first.y_rank()
			: first.x_rank();

		// Hidden variable degree (first polynomial)
		auto d0_h = (hidden_var == Variable::X)
			? first.x_rank()
			: first.y_rank();

		// Non-hidden variable degree (second polynomial)
		auto d1 = (hidden_var == Variable::X)
			? second.y_rank()
			: second.x_rank();

		// Hidden variable degree (second polynomial)
		auto d1_h = (hidden_var == Variable::X)
			? second.x_rank()
			: second.y_rank();

		// First line of first polynomial (we construct by last-to-first, then
		// reverse the elements, because it makes the code cleaner this way).
		for (BivariatePolynomial::Index i = 0; i <= d0; i++) {
			auto polynomial = make_shared<UnivariatePolynomial>(d0_h);
			for (BivariatePolynomial::Index j = 0; j <= d0_h; j++) {
				if (hidden_var == Variable::Y) {
					(*polynomial)[j] = first(i, j);
				} else {
					(*polynomial)[j] = first(j, i);
				}
			}

			s(0, i) = std::move(polynomial);
		}
		std::reverse(s.begin_from(0, 0), s.end_upto(0, d0));
		for (BivariatePolynomial::Index i = 1; i < d1; i++) {
			// Ah, the glory of the `shared_ptr`. This seemingly expensive
			// operation just increments some reference counters, without
			// having to construct a copy of each polynomial every time.
			std::copy_n(s.begin_from(0, 0),
					d0 + 1,
					s.begin_from(i, i));
		}

		// First line of second polynomial (same stuff about construction
		// and reversing apply here).
		size_t second_poly_start = d1;
		for (BivariatePolynomial::Index i = 0; i <= d1; i++) {
			auto polynomial = make_shared<UnivariatePolynomial>(d1_h);
			for (BivariatePolynomial::Index j = 0; j <= d1_h; j++) {
				if (hidden_var == Variable::Y) {
					(*polynomial)[j] = second(i, j);
				} else {
					(*polynomial)[j] = second(j, i);
				}
			}

			s(second_poly_start, i) = std::move(polynomial);
		}

		std::reverse(s.begin_from(second_poly_start, 0), s.end_upto(second_poly_start, d1));
		for (BivariatePolynomial::Index i = 1; i < d0; i++) {
			// Glorious shared_ptr is glorious
			std::copy_n(s.begin_from(second_poly_start,0),
					d1 + 1,
					s.begin_from(second_poly_start + i, i));
		}
	}

	SylvesterPolynomial SylvesterPolynomial::from_sylvester_matrix(const SylvesterMatrix& matrix)
	{
		SylvesterPolynomial polynomial(matrix.hidden_variable_degree(),
				matrix.degree(), matrix.hidden_variable());

		for (size_t coeff = 0; coeff <= polynomial.degree(); coeff++) {
			auto& polynomial_mat = polynomial.matrix_coeffs[coeff];
			for (size_t i = 0; i < polynomial.dimension(); i++) {
				for (size_t j = 0; j < polynomial.dimension(); j++) {
					double val = matrix(i, j).at(coeff);
					polynomial_mat(i, j) = val;
				}
			}
		}

		return polynomial;
	}

	SylvesterPolynomial SylvesterPolynomial::var_change(int t_1, int t_2, int t_3, int t_4)
	{
		SylvesterPolynomial result(degree(), dimension(), hidden_var);
		BinomialCoefficients precomputed = BinomialCoefficients::up_to(degree());

		for (size_t i = 0; i <= degree(); i++) {
			UnivariatePolynomial first = UnivariatePolynomial::first_degree_poly_pow(
					t_1, t_2, i, precomputed);
			UnivariatePolynomial second = UnivariatePolynomial::first_degree_poly_pow(
					t_3, t_4, degree() - i, precomputed);

			auto poly_result = first * second;

			const auto& mat = matrix_coeffs[i];
			for (size_t k = 0; k <= degree(); k++) {
				result[k] += mat * poly_result[k];
			}
		}

		return result;
	}
}

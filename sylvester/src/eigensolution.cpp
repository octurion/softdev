#include "eigensolution.hpp"

#include <Eigen/Eigenvalues>

#include <algorithm>
#include <stdexcept>
#include <tuple>
#include <vector>
#include <utility>

#include <iostream>

/*
 * We couldn't figure out how to use LAPACKE with CMake, so instead we'll
 * just use the LAPACK equivalent function directly.
 */
extern "C" void dggev_(const char* JOBVL, const char* JOBVR, const int* N,
                       const double* A, const int* LDA, const double* B, const int* LDB,
                       double* ALPHAR, double* ALPHAI, double* BETA,
                       double* VL, const int* LDVL, double* VR, const int* LDVR,
                       double* WORK, const int* LWORK, int* INFO);

namespace
{
	const double IMAGINARY_PART_EPSILON = 1e-20;
	const double Y_CLOSENESS_EPSILON = 1e-20;
}

namespace softdev {
	using Eigen::EigenSolver;

	using Eigen::MatrixXd;
	using Eigen::VectorXd;

	using Eigen::MatrixXcd;
	using Eigen::VectorXcd;

	using std::isfinite;
	using std::complex;
	using std::vector;

	void add_solution(vector<XValues>& solutions, complex<double> x, complex<double> y);
	bool GEP(const MatrixXd& A, const MatrixXd& B, MatrixXd& v, MatrixXd& lambda);

	/**
	 * Eigen-wrapper around `dggev_`, shamelessly copied from the Eigen wiki.
	 */
	bool GEP(const MatrixXd& A, const MatrixXd& B, MatrixXd& v, MatrixXd& lambda)
	{
		int N = A.cols(); // Number of columns of A and B. Number of rows of v.
		if (B.cols() != N  || A.rows()!=N || B.rows()!=N)
			return false;

		v.resize(N,N);
		lambda.resize(N, 3);

		int LDA = A.outerStride();
		int LDB = B.outerStride();
		int LDV = v.outerStride();

		double WORKDUMMY;
		int LWORK = -1; // Request optimum work size.
		int INFO = 0;

		double * alphar = const_cast<double*>(lambda.col(0).data());
		double * alphai = const_cast<double*>(lambda.col(1).data());
		double * beta   = const_cast<double*>(lambda.col(2).data());

		// Get the optimum work size.
		dggev_("N", "V", &N, A.data(), &LDA, B.data(), &LDB, alphar, alphai, beta, 0, &LDV, v.data(), &LDV, &WORKDUMMY, &LWORK, &INFO);

		LWORK = int(WORKDUMMY) + 32;
		VectorXd WORK(LWORK);

		dggev_("N", "V", &N, A.data(), &LDA, B.data(), &LDB, alphar, alphai, beta, 0, &LDV, v.data(), &LDV, WORK.data(), &LWORK, &INFO);

		return INFO==0;
	}

	MatrixXd companion_matrix_of(const SylvesterPolynomial& polynomial)
	{
		const auto& max_degree_mat = polynomial.max_degree_mat();

		auto dimension = max_degree_mat.rows();
		auto degree = polynomial.degree();

		MatrixXd companion_matrix(degree * dimension, degree * dimension);
		companion_matrix.setZero();

		MatrixXd neg_inverted = -max_degree_mat.inverse();

		MatrixXd::Index identity_dim = (degree - 1) * dimension;
		companion_matrix.topRightCorner(identity_dim, identity_dim).setIdentity();
		for (size_t i = 0; i < degree; i++) {
			MatrixXd::Index row_start = (degree - 1) * dimension;
			MatrixXd::Index col_start = i * dimension;

			companion_matrix.block(row_start, col_start, dimension, dimension)
				= neg_inverted * polynomial[i];
		}

		return companion_matrix;
	}

	CoefficientInfo sylvester_polynomial_info(const SylvesterPolynomial& polynomial)
	{
		const auto& max_degree_mat = polynomial.max_degree_mat();

		auto eigenvalues = max_degree_mat.eigenvalues();

		auto singular_values_squared = eigenvalues.rowwise().squaredNorm();

		auto max_coeff = sqrt(singular_values_squared.maxCoeff());
		auto min_coeff = sqrt(singular_values_squared.minCoeff());

		return CoefficientInfo(min_coeff, max_coeff);
	}

	std::tuple<MatrixXd, MatrixXd> generalized_eigenproblem_matrices_of(
			const SylvesterPolynomial& polynomial)
	{
		const auto& max_degree_mat = polynomial.max_degree_mat();

		auto dimension = max_degree_mat.rows();
		auto degree = polynomial.degree();

		MatrixXd::Index identity_dim = (degree - 1) * dimension;

		MatrixXd l_0(degree * dimension, degree * dimension);
		l_0.setZero();
		l_0.topRightCorner(identity_dim, identity_dim).diagonal().setConstant(-1);
		for (size_t i = 0; i < degree; i++) {
			MatrixXd::Index row_start = (degree - 1) * dimension;
			MatrixXd::Index col_start = i * dimension;

			l_0.block(row_start, col_start, dimension, dimension) = polynomial[i];
		}

		MatrixXd l_1(degree * dimension, degree * dimension);
		l_1.setZero();
		l_1.topLeftCorner(identity_dim, identity_dim).diagonal().setConstant(-1);
		l_1.bottomRightCorner(dimension, dimension) -= max_degree_mat;

		return std::make_tuple(std::move(l_1), std::move(l_0));
	}

	EigenSolution sylvester_polynomial_eigensolution(
			const SylvesterPolynomial& polynomial,
			const CoefficientInfo& info,
			double bound_exp)
	{
		auto dimension = polynomial.max_degree_mat().rows();

		bool min_coeff_too_small = log10(info.min_coefficient()) < -bound_exp;
		bool kappa_too_big = log10(info.kappa()) > bound_exp;

		EigenSolution solution;
		if (min_coeff_too_small || kappa_too_big) {
			MatrixXd l_0;
			MatrixXd l_1;

			std::tie(l_1, l_0) = generalized_eigenproblem_matrices_of(polynomial);

			MatrixXd pseudo_eigenvectors;
			MatrixXd pseudo_eigenvalues;

			GEP(l_0, l_1, pseudo_eigenvectors, pseudo_eigenvalues);

			MatrixXcd eigenvectors(dimension, pseudo_eigenvectors.cols());
			VectorXcd eigenvalues(pseudo_eigenvalues.rows());

			eigenvectors.real() = pseudo_eigenvectors.topRows(dimension);
			eigenvectors.imag().setZero();

			auto div_by = pseudo_eigenvalues.col(2);

			eigenvalues.real() = pseudo_eigenvalues.col(0).cwiseQuotient(div_by);
			eigenvalues.imag() = pseudo_eigenvalues.col(1).cwiseQuotient(div_by);

			solution.eigenvalues = std::move(eigenvalues);
			solution.eigenvectors = std::move(eigenvectors);
			solution.issue = min_coeff_too_small
				? SylvesterPolynomialIssue::MinCoeffTooSmall
				: SylvesterPolynomialIssue::KappaTooBig;
		} else {
			auto companion_matrix = companion_matrix_of(polynomial);

			EigenSolver<MatrixXd> eigensolver;
			eigensolver.compute(companion_matrix, true);

			auto eigenvalues = eigensolver.eigenvalues();
			auto eigenvectors = eigensolver.eigenvectors();

			if (eigenvalues.rows() == 0) {
				throw std::runtime_error("Could not invert companion matrix"
						" even though Kappa is bounded and minimum singular"
						" value is large enough");
			}

			MatrixXcd eigenvectors_chopped(dimension, eigenvectors.cols());
			eigenvectors_chopped = eigenvectors.topRows(dimension);

			solution.eigenvalues = std::move(eigenvalues);
			solution.eigenvectors = std::move(eigenvectors_chopped);
			solution.issue = SylvesterPolynomialIssue::None;
		}

		return solution;
	}

	YValues YValues::from_eigenvectors(const EigenSolution& solutions)
	{
		vector<XValues> y_solutions;

		for (MatrixXcd::Index i = 0; i < solutions.eigenvalues.rows(); i++) {
			auto eigenvalue = solutions.eigenvalues[i];
			auto eigenvector = solutions.eigenvectors.col(i);

			if (fabs(eigenvalue.imag()) > IMAGINARY_PART_EPSILON) {
				VectorXcd e = eigenvector;
				if (solutions.issue != softdev::SylvesterPolynomialIssue::None) {
					e += complex<double>(0, 1) * solutions.eigenvectors.col(i + 1);
				}
				auto last_elem = e[e.rows() - 1];
				auto y = eigenvalue;

				i++;

				auto x = e[e.rows() - 2] / last_elem;

				add_solution(y_solutions, x, y);
				add_solution(y_solutions, std::conj(x), std::conj(y));
			} else {
				auto last_elem = eigenvector[eigenvector.rows() - 1];

				auto x = eigenvector[eigenvector.rows() - 2] / last_elem;
				auto y = eigenvalue;

				add_solution(y_solutions, x, y);
			}
		}

		return y_solutions;
	}

	/**
	 * Determines the proximity of a y variable to any other one currently
	 * inserted. If no other y variable is found that is close, adds it
	 * to the solution set, as well as its (currently unique) x value.
	 *
	 * Otherwise, it adds it to the first close y value found.
	 */
	void add_solution(vector<XValues>& solutions, complex<double> x, complex<double> y)
	{
		auto it = std::find_if(solutions.begin(), solutions.end(),
			[&y](const XValues& values) {
				return std::abs(values.first - y) < Y_CLOSENESS_EPSILON;
			});

		if (it == solutions.end()) {
			solutions.emplace_back(y, vector<complex<double>> {x});
		} else {
			it->second.push_back(x);
		}
	}

	void Roots::insert(double eigenvalue, double eigenroot, double eigenvalue_closeness_epsilon)
	{
		auto it = std::find_if(roots.begin(), roots.end(),
			[&eigenvalue, &eigenvalue_closeness_epsilon](const EigenRoots& roots) {
				return std::abs(roots.first - eigenvalue) < eigenvalue_closeness_epsilon;
			});

		if (it == roots.end()) {
			roots.emplace_back(eigenvalue, vector<double> {eigenroot});
		} else {
			it->second.push_back(eigenroot);
		}
	}

	Roots Roots::from_eigenvectors(const EigenSolution& solutions,
			double eigenvalue_closeness_epsilon)
	{
		Roots result;

		for (MatrixXcd::Index i = 0; i < solutions.eigenvalues.rows(); i++) {
			auto eigenvalue = solutions.eigenvalues[i];
			auto eigenvector = solutions.eigenvectors.col(i);

			if (!isfinite(eigenvalue.real()) || !isfinite(eigenvalue.imag())
						|| eigenvalue.imag() != 0) {
				continue;
			}

			auto last_elem = eigenvector[eigenvector.rows() - 1];
			auto potential_eigenroot = eigenvector[eigenvector.rows() - 2] / last_elem;

			double actual_eigenroot = potential_eigenroot.real();
			if (!isfinite(potential_eigenroot.real()) || !isfinite(potential_eigenroot.imag())
					|| potential_eigenroot.imag() != 0) {
				actual_eigenroot = std::numeric_limits<double>::quiet_NaN();
			}

			double actual_eigenvalue = eigenvalue.real();

			result.insert(actual_eigenvalue, actual_eigenroot, eigenvalue_closeness_epsilon);
		}

		return result;
	}

	std::vector<std::pair<double, double>> Roots::root_pairs(
			const BivariatePolynomial& first,
			const BivariatePolynomial& second,
			const SylvesterPolynomial& poly,
			double root_epsilon) const
	{
		std::vector<std::pair<double, double>> pairs;

		auto hidden_var = poly.hidden_variable();

		for (const auto& pair : roots) {
			if (pair.second.size() == 1 && isfinite(pair.second.front())) {
				if (hidden_var == Variable::Y) {
					pairs.emplace_back(pair.second.front(), pair.first);
				} else {
					pairs.emplace_back(pair.first, pair.second.front());
				}

				continue;
			}

			auto first_univariate = hidden_var == Variable::X
				? first.evaluate_for_x(pair.first)
				: first.evaluate_for_y(pair.first);

			auto second_univariate = hidden_var == Variable::X
				? second.evaluate_for_x(pair.first)
				: second.evaluate_for_y(pair.first);

			auto first_roots = first_univariate.real_roots();
			for (auto e : first_roots) {
				if (std::abs(second_univariate.evaluate_for(e)) >= root_epsilon) {
					continue;
				}

				if (hidden_var == Variable::X) {
					pairs.emplace_back(pair.first, e);
				} else {
					pairs.emplace_back(e, pair.first);
				}
			}
		}

		return pairs;
	}

	void YValues::print_acceptable_solutions(FILE* out,
			const BivariatePolynomial& first,
			const BivariatePolynomial& second,
			double epsilon) const
	{
		bool found_solutions = false;

		for (auto y_it = y_solutions.cbegin(); y_it != y_solutions.cend(); y_it++) {
			const auto& x_solutions = y_it->second;

			const auto& y = y_it->first;
			if (!std::isfinite(y.real()) || !std::isfinite(y.imag())) {
				continue;
			}

			if (x_solutions.size() > 1) {
				found_solutions = true;
				fprintf(out, "- Solution y ~= %e%+ei has a multiplicity of %zu\n",
						y_it->first.real(), y_it->first.imag(), x_solutions.size());
				continue;
			}

			const auto& x = x_solutions.front();
			if (!std::isfinite(x.real()) || !std::isfinite(x.imag())) {
				continue;
			}

			fprintf(out, "- y ~= %.4e %+.4ei, x ~= %.4e %+.4ei\n",
					y.real(), y.imag(), x.real(), x.imag());
			found_solutions = true;

			auto first_eval = first.evaluate_for(x, y);
			auto second_eval = second.evaluate_for(x, y);

			fprintf(out, "  P(x, y) = %.4e %+.4ei -> %s\n",
					first_eval.real(), first_eval.imag(),
					std::abs(first_eval) < epsilon
					? "Acceptable"
					: "Unacceptable");

			fprintf(out, "  Q(x, y) = %.4e %+.4ei -> %s\n",
					second_eval.real(), second_eval.imag(),
					std::abs(second_eval) < epsilon
					? "Acceptable"
					: "Unacceptable");
		}

		if (!found_solutions) {
			fprintf(out, "No solutions found\n");
		}
	}

	EigenSolution revert_variable_change(EigenSolution solution,
			int t_1, int t_2, int t_3, int t_4)
	{
		auto nominator = t_1 * solution.eigenvalues + VectorXcd::Constant(
				solution.eigenvalues.rows(), t_2);
		auto denominator = t_3 * solution.eigenvalues + VectorXcd::Constant(
				solution.eigenvalues.rows(), t_4);
		solution.eigenvalues = nominator.cwiseQuotient(denominator);

		return solution;
	}
}

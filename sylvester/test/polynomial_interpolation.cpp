#include "gtest_util.hpp"

#include "polynomial.hpp"

#include <vector>

using namespace Eigen;

using namespace softdev;
using namespace std;
using namespace testing;

TEST(Polynomial, Interpolation1)
{
	vector<pair<double, double>> dataset =
	{
		{-1, 0}, {4, -1}, {-1, -5}, {-4, 2}, {4, -4},
	};

	auto given = interpolation_matrix(2, dataset);

	MatrixXd expected(5, 6);
	expected <<
		1, -1,  0,  1,   0,  0,
		1,  4, -1, 16,  -4,  1,
		1, -1, -5,  1,   5, 25,
		1, -4,  2, 16,  -8,  4,
		1,  4, -4, 16, -16, 16;

	EXPECT_THAT(given, MatrixApproximateTo(expected, 1e-5));

	MatrixXd expected_grid(3, 3);
	expected_grid <<
		  2.733333333, 5, 1,
		  2.250000000, 0, 0,
		-0.4833333333, 0, 0;

	auto poly = BivariatePolynomial::from_interpolation(2, dataset);

	EXPECT_THAT(poly.as_grid(), MatrixApproximateTo(expected_grid, 1e-5));
}

TEST(Polynomial, Interpolation2)
{
	vector<pair<double, double>> dataset =
	{
		{-1, -5}, {-3, 3}, {-4, -4}, {-3, -1}, {-2, -2}
	};

	auto given = interpolation_matrix(2, dataset);

	MatrixXd expected(5, 6);
	expected <<
		1, -1, -5,  1,  5, 25,
		1, -3,  3,  9, -9,  9,
		1, -4, -4, 16, 16, 16,
		1, -3, -1,  9,  3,  1,
		1, -2, -2,  4,  4,  4;

	EXPECT_THAT(given, MatrixApproximateTo(expected, 1e-5));

	MatrixXd expected_grid(3, 3);
	expected_grid <<
		  -96, -3.5, 1,
		-68.5, -0.5, 0,
		-12.5,    0, 0;

	auto poly = BivariatePolynomial::from_interpolation(2, dataset);

	EXPECT_THAT(poly.as_grid(), MatrixApproximateTo(expected_grid, 1e-5));
}

TEST(Polynomial, Interpolation3)
{
	vector<pair<double, double>> dataset =
	{
		{-1, -5}, {-1, 0}, {-3, 2}, {3, 3}, {-1, -3}
	};

	auto given = interpolation_matrix(2, dataset);

	MatrixXd expected(5, 6);
	expected <<
		1, -1, -5, 1,  5, 25,
		1, -1,  0, 1,  0,  0,
		1, -3,  2, 9, -6,  4,
		1,  3,  3, 9,  9,  9,
		1, -1, -3, 1,  3,  9;

	EXPECT_THAT(given, MatrixApproximateTo(expected, 1e-5));

	MatrixXd expected_grid(3, 2);
	expected_grid <<
				 -2.5, 1,
		 -2.666666667, 1,
		-0.1666666667, 0;

	auto poly = BivariatePolynomial::from_interpolation(2, dataset);

	EXPECT_THAT(poly.as_grid(), MatrixApproximateTo(expected_grid, 1e-5));
}

TEST(Polynomial, Interpolation4)
{
	vector<pair<double, double>> dataset =
	{
		{-5, 3}, {2, 4}, {5, 1}, {-3, 1}, {-3, 2}
	};

	auto given = interpolation_matrix(2, dataset);

	MatrixXd expected(5, 6);
	expected <<
		1, -5, 3, 25, -15,  9,
		1,  2, 4,  4,   8, 16,
		1,  5, 1, 25,   5,  1,
		1, -3, 1,  9,  -3,  1,
		1, -3, 2,  9,  -6,  4;

	EXPECT_THAT(given, MatrixApproximateTo(expected, 1e-5));

	MatrixXd expected_grid(3, 3);
	expected_grid <<
		  7.25, -4.875, 1,
		 1.075, -0.625, 0,
		-0.225,      0, 0;

	auto poly = BivariatePolynomial::from_interpolation(2, dataset);

	EXPECT_THAT(poly.as_grid(), MatrixApproximateTo(expected_grid, 1e-5));
}

TEST(Polynomial, Interpolation5)
{
	vector<pair<double, double>> dataset =
	{
		{-5, 1}, {-5, 0}, {2, -2}, {-5, -2}, {4, 4}
	};

	auto given = interpolation_matrix(2, dataset);

	MatrixXd expected(5, 6);
	expected <<
		1, -5,  1, 25, -5,  1,
		1, -5,  0, 25,  0,  0,
		1,  2, -2,  4, -4,  4,
		1, -5, -2, 25, 10,  4,
		1,  4,  4, 16, 16, 16;

	EXPECT_THAT(given, MatrixApproximateTo(expected, 1e-5));

	MatrixXd expected_grid(3, 2);
	expected_grid <<
		40, 5,
		-7, 1,
		-3, 0;

	auto poly = BivariatePolynomial::from_interpolation(2, dataset);

	EXPECT_THAT(poly.as_grid(), MatrixApproximateTo(expected_grid, 1e-5));
}

TEST(Polynomial, Interpolation6)
{
	vector<pair<double, double>> dataset =
	{
		{-3, -4}, {0, 4}, {-5, 0}, {1, -5}, {-4, 3}
	};

	auto given = interpolation_matrix(2, dataset);

	MatrixXd expected(5, 6);
	expected <<
		1, -3, -4,  9,  12, 16,
		1,  0,  4,  0,   0, 16,
		1, -5,  0, 25,   0,  0,
		1,  1, -5,  1,  -5, 25,
		1, -4,  3, 16, -12,  9;

	EXPECT_THAT(given, MatrixApproximateTo(expected, 1e-5));

	MatrixXd expected_grid(3, 3);
	expected_grid <<
		-20.56042032,  1.140105079, 1,
		 1.649737303, 0.3082311734, 0,
		 1.152364273,            0, 0;

	auto poly = BivariatePolynomial::from_interpolation(2, dataset);

	EXPECT_THAT(poly.as_grid(), MatrixApproximateTo(expected_grid, 1e-5));
}

TEST(Polynomial, Interpolation7)
{
	vector<pair<double, double>> dataset =
	{
		{-1, 0}, {1, 0}, {3, -1}, {4, -1}, {1, -3}
	};

	auto given = interpolation_matrix(2, dataset);

	MatrixXd expected(5, 6);
	expected <<
		1.0, -1.0,  0.0,  1.0, -0.0, 0.0,
		1.0,  1.0,  0.0,  1.0,  0.0, 0.0,
		1.0,  3.0, -1.0,  9.0, -3.0, 1.0,
		1.0,  4.0, -1.0, 16.0, -4.0, 1.0,
		1.0,  1.0, -3.0,  1.0, -3.0, 9.0;

	EXPECT_THAT(given, MatrixApproximateTo(expected, 1e-5));

	MatrixXd expected_grid(3, 3);
	expected_grid <<
		   0.33333333333333304,  5.333333333333334, 1,
		-4.522777457663917e-16, -2.333333333333335, 0,
		   -0.3333333333333335,                  0, 0;

	auto poly = BivariatePolynomial::from_interpolation(2, dataset);

	EXPECT_THAT(poly.as_grid(), MatrixApproximateTo(expected_grid, 1e-5));
}

TEST(Polynomial, ThirdDegreeInterpolation)
{
	vector<pair<double, double>> dataset =
	{
		{-3, 2}, {-1, 6}, {2, 8}, {3, 7}, {5,1}, {2,2}, {10, -3}, {0, 8}, {6, 7}
	};

	auto given = interpolation_matrix(3, dataset);

	MatrixXd expected(9, 10);
	expected <<
		1.0, -3.0,  2.0,   9.0,  -6.0,  4.0,  -27.0,   18.0, -12.0,   8.0,
		1.0, -1.0,  6.0,   1.0,  -6.0, 36.0,   -1.0,    6.0, -36.0, 216.0,
		1.0,  2.0,  8.0,   4.0,  16.0, 64.0,    8.0,   32.0, 128.0, 512.0,
		1.0,  3.0,  7.0,   9.0,  21.0, 49.0,   27.0,   63.0, 147.0, 343.0,
		1.0,  5.0,  1.0,  25.0,   5.0,  1.0,  125.0,   25.0,   5.0,   1.0,
		1.0,  2.0,  2.0,   4.0,   4.0,  4.0,    8.0,    8.0,   8.0,   8.0,
		1.0, 10.0, -3.0, 100.0, -30.0,  9.0, 1000.0, -300.0,  90.0, -27.0,
		1.0,  0.0,  8.0,   0.0,   0.0, 64.0,    0.0,    0.0,   0.0, 512.0,
		1.0,  6.0,  7.0,  36.0,  42.0, 49.0,  216.0,  252.0, 294.0, 343.0;

	EXPECT_THAT(given, MatrixApproximateTo(expected, 1e-5));

	MatrixXd expected_grid(4, 4);
	expected_grid <<
		-110.74331867197213,   87.14523427020228, -17.16278992952565,   1,
		-11.821820792711605,   4.930155147928036, -0.30985305186432277, 0,
		 1.5606700382450325, -0.7951460583785283,                    0, 0,
		0.45304294654302574,                   0,                    0, 0;

	auto poly = BivariatePolynomial::from_interpolation(3, dataset);

	EXPECT_THAT(poly.as_grid(), MatrixApproximateTo(expected_grid, 1e-5));
}

#include "gtest_util.hpp"

#include <cmath>

#include "polynomial.hpp"

using namespace softdev;
using namespace testing;

TEST(Polynomial, TrivialMultiplication)
{
	UnivariatePolynomial first  = {0, 3}; // 3x
	UnivariatePolynomial second = {0, 3}; // 3x

	auto result = first * second;

	EXPECT_THAT(result.degree(), Eq(first.degree() + second.degree()));
	EXPECT_THAT(result[0], Eq(0));
	EXPECT_THAT(result[1], Eq(0));
	EXPECT_THAT(result[2], Eq(9));
}

TEST(Polynomial, SimpleExponentiation)
{
	BinomialCoefficients coeffs = BinomialCoefficients::up_to(5);

	auto foo = UnivariatePolynomial::first_degree_poly_pow(3, 0, 1, coeffs);

	EXPECT_THAT(foo.degree(), Eq(1u));

	EXPECT_THAT(foo[0], Eq(0));
	EXPECT_THAT(foo[1], Eq(3));
}

TEST(Polynomial, ConstantExponentiation)
{
	BinomialCoefficients coeffs = BinomialCoefficients::up_to(5);

	auto bar = UnivariatePolynomial::first_degree_poly_pow(0, 5, 3, coeffs);
	EXPECT_THAT(bar[0], Eq(125));
}

TEST(Polynomial, ComplicatedExponentiation)
{
	BinomialCoefficients coeffs = BinomialCoefficients::up_to(5);

	auto quux = UnivariatePolynomial::first_degree_poly_pow(7, 11, 3, coeffs);

	EXPECT_THAT(quux.degree(), Eq(3));

	EXPECT_THAT(quux[0], Eq(1331));
	EXPECT_THAT(quux[1], Eq(2541));
	EXPECT_THAT(quux[2], Eq(1617));
	EXPECT_THAT(quux[3], Eq(343));
}

TEST(Polynomial, ExponentiationWithMultiplication)
{
	BinomialCoefficients coeffs = BinomialCoefficients::up_to(5);

	auto first = UnivariatePolynomial::first_degree_poly_pow(7, 11, 3, coeffs);
	auto second = UnivariatePolynomial::first_degree_poly_pow(1, 3, 2, coeffs);

	auto result = first * second;

	EXPECT_THAT(result.degree(), Eq(5u));

	EXPECT_THAT(result[0], Eq(11979));
	EXPECT_THAT(result[1], Eq(30855));
	EXPECT_THAT(result[2], Eq(31130));
	EXPECT_THAT(result[3], Eq(15330));
	EXPECT_THAT(result[4], Eq(3675));
	EXPECT_THAT(result[5], Eq(343));

	auto commutative = second * first;

	EXPECT_THAT(commutative.degree(), Eq(5u));

	EXPECT_THAT(commutative[0], Eq(11979));
	EXPECT_THAT(commutative[1], Eq(30855));
	EXPECT_THAT(commutative[2], Eq(31130));
	EXPECT_THAT(commutative[3], Eq(15330));
	EXPECT_THAT(commutative[4], Eq(3675));
	EXPECT_THAT(commutative[5], Eq(343));
}

TEST(Polynomial, SecondDegreeRoots)
{
	UnivariatePolynomial poly = {-1, 0, 1}; // x^2 - 1

	auto roots = poly.real_roots();

	EXPECT_THAT(roots, UnorderedElementsAre(
				DoubleNear(1, 1e-5),
				DoubleNear(-1, 1e-5)));
}

TEST(Polynomial, ThirdDegreeRoots)
{
	UnivariatePolynomial poly = {-24, 26, -9, 1}; // (x-2)(x-3)(x-4)

	auto roots = poly.real_roots();

	EXPECT_THAT(roots, UnorderedElementsAre(
				DoubleNear(2.0, 1e-5),
				DoubleNear(3.0, 1e-5),
				DoubleNear(4.0, 1e-5)));
}

TEST(Polynomial, ThirdDegreeApproxRoots)
{
	UnivariatePolynomial poly = {6, -2, -3, 1}; // x^3 - 3x^2 - 2x + 6

	auto roots = poly.real_roots();

	EXPECT_THAT(roots, UnorderedElementsAre(
				DoubleNear(3.0, 1e-5),
				DoubleNear(-sqrt(2), 1e-5),
				DoubleNear(sqrt(2), 1e-5)));
}

TEST(Polynomial, SecondDegreeComplexRoots)
{
	UnivariatePolynomial poly = {1, 0, 1}; // x^2 + 1

	auto roots = poly.real_roots();

	EXPECT_THAT(roots, IsEmpty());
}

TEST(Polynomial, ZeroDegreeException)
{
	UnivariatePolynomial poly = {0};

	EXPECT_ANY_THROW(poly.real_roots());
}

TEST(Polynomial, NonUnitaryLargestCoefficientDegree)
{
	UnivariatePolynomial poly = {-6, 2, 3, -1}; // -x^3 + 3x^2 + 2x - 6

	auto roots = poly.real_roots();

	EXPECT_THAT(roots, UnorderedElementsAre(
				DoubleNear(3.0, 1e-5),
				DoubleNear(-sqrt(2), 1e-5),
				DoubleNear(sqrt(2), 1e-5)));
}

TEST(Polynomial, LargestCoefficientsAreZero)
{
	UnivariatePolynomial poly = {-6, 2, 3, -1, 0, 0, 0, 0}; // -x^3 + 3x^2 + 2x - 6

	auto roots = poly.real_roots();

	EXPECT_THAT(roots, UnorderedElementsAre(
				DoubleNear(3.0, 1e-5),
				DoubleNear(-sqrt(2), 1e-5),
				DoubleNear(sqrt(2), 1e-5)));
}

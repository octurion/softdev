#include "gtest_util.hpp"

#include <cmath>

#include "polynomial.hpp"

using namespace std;
using namespace softdev;
using namespace testing;

TEST(Polynomial, VariableEvaluation)
{
	auto polynomial = BivariatePolynomial::parse("-1-2x-3y-4x^2-5xy-6y^2");
	auto x_polynomial = polynomial.evaluate_for_y(0);
	auto y_polynomial = polynomial.evaluate_for_x(0);

	vector<double> x_vec(x_polynomial.begin(), x_polynomial.end());
	vector<double> y_vec(y_polynomial.begin(), y_polynomial.end());

	EXPECT_THAT(x_vec, ElementsAre(-1, -2, -4));
	EXPECT_THAT(y_vec, ElementsAre(-1, -3, -6));

	EXPECT_THAT(polynomial.evaluate_for(2, 6), Eq(-315));
}

TEST(Polynomial, VariableEvaluationHarder)
{
	auto polynomial = BivariatePolynomial::parse("x^2+y-2x^3 + xy + xy^2");

	auto x_polynomial = polynomial.evaluate_for_y(3);
	auto y_polynomial = polynomial.evaluate_for_x(5);

	vector<double> x_vec(x_polynomial.begin(), x_polynomial.end());
	vector<double> y_vec(y_polynomial.begin(), y_polynomial.end());

	EXPECT_THAT(x_vec, ElementsAre(3, 12, 1, -2));
	EXPECT_THAT(y_vec, ElementsAre(-225, 6, 5));

	EXPECT_THAT(polynomial.evaluate_for(4, -2), Eq(-106));
}

TEST(Polynomial, VariableEvaluationLeadingZeroes)
{
	auto polynomial = BivariatePolynomial::parse("y^5");

	auto x_polynomial = polynomial.evaluate_for_y(2);
	auto y_polynomial = polynomial.evaluate_for_x(20);

	vector<double> x_vec(x_polynomial.begin(), x_polynomial.end());
	vector<double> y_vec(y_polynomial.begin(), y_polynomial.end());

	EXPECT_THAT(y_vec, ElementsAre(0, 0, 0, 0, 0, 1));
	EXPECT_THAT(x_vec, ElementsAre(32));

	EXPECT_THAT(polynomial.evaluate_for(5, 2), Eq(32));
	EXPECT_THAT(polynomial.evaluate_for(12, 2), Eq(32));
}

TEST(Polynomial, Evaluation)
{
	auto poly = BivariatePolynomial::parse("y*x^2 + 4*x -y +5");

	EXPECT_THAT(poly.evaluate_for(-1.10874, -2.4641), DoubleNear(0, 1e-4));
	EXPECT_THAT(poly.evaluate_for(-0.163986, 4.4641), DoubleNear(0, 1e-4));
}

TEST(Polynomial, Evaluation2)
{
	auto poly = BivariatePolynomial::parse("x^2+5y-8");

	EXPECT_THAT(poly.evaluate_for( sqrt(2) / 2, 1.5), DoubleNear(0, 1e-5));
	EXPECT_THAT(poly.evaluate_for(-sqrt(2) / 2, 1.5), DoubleNear(0, 1e-5));
}

#include "gtest_util.hpp"

#include "binomial.hpp"
#include "sylvester.hpp"
#include "eigensolution.hpp"

using namespace Eigen;
using namespace softdev;
using namespace std;

using namespace testing;

TEST(Eigensolution, ExampleSolution1)
{
	auto first_poly = BivariatePolynomial::parse("x^2-y+1");
	auto second_poly = BivariatePolynomial::parse("x^2+5y-8");

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);
	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	auto coeff_info = sylvester_polynomial_info(poly);
	auto eigensolution = sylvester_polynomial_eigensolution(poly, coeff_info, 7);
	auto rootlist = Roots::from_eigenvectors(eigensolution,
			std::numeric_limits<float>::epsilon());

	auto roots = rootlist.root_pairs(first_poly, second_poly, poly,
			std::numeric_limits<float>::epsilon());

	EXPECT_THAT(roots, UnorderedElementsAre(
		Pair(DoubleNear( sqrt(2) / 2, 1e-5), DoubleNear(1.5, 1e-5)),
		Pair(DoubleNear(-sqrt(2) / 2, 1e-5), DoubleNear(1.5, 1e-5))
	));
}

TEST(Eigensolution, SymmetricExampleSolution1)
{
	auto first_poly = BivariatePolynomial::parse("y^2-x+1");
	auto second_poly = BivariatePolynomial::parse("y^2+5x-8");

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);
	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	auto coeff_info = sylvester_polynomial_info(poly);
	auto eigensolution = sylvester_polynomial_eigensolution(poly, coeff_info, 7);
	auto rootlist = Roots::from_eigenvectors(eigensolution,
			std::numeric_limits<float>::epsilon());

	auto roots = rootlist.root_pairs(first_poly, second_poly, poly,
			std::numeric_limits<float>::epsilon());

	EXPECT_THAT(roots, UnorderedElementsAre(
		Pair(DoubleNear(1.5, 1e-5), DoubleNear( sqrt(2) / 2, 1e-5)),
		Pair(DoubleNear(1.5, 1e-5), DoubleNear(-sqrt(2) / 2, 1e-5))
	));
}

TEST(Eigensolution, ExampleSolution2)
{
	auto first_poly = BivariatePolynomial::parse("2*y*x + y - 3");
	auto second_poly = BivariatePolynomial::parse("y*x^2 + 4*x -y +5");

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);
	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	auto coeff_info = sylvester_polynomial_info(poly);
	auto eigensolution = sylvester_polynomial_eigensolution(poly, coeff_info, 7);
	auto rootlist = Roots::from_eigenvectors(eigensolution,
			std::numeric_limits<float>::epsilon());

	auto roots = rootlist.root_pairs(first_poly, second_poly, poly,
			std::numeric_limits<float>::epsilon());

	EXPECT_THAT(roots, UnorderedElementsAre(
		Pair(DoubleNear(-1.10874, 1e-5), DoubleNear(-2.4641, 1e-5)),
		Pair(DoubleNear(-0.163986, 1e-5), DoubleNear(4.4641, 1e-5))
	));
}

TEST(Eigensolution, SymmetricExampleSolution2)
{
	auto first_poly = BivariatePolynomial::parse("2*x*y + x - 3");
	auto second_poly = BivariatePolynomial::parse("x*y^2 + 4*y -x +5");

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);
	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	auto coeff_info = sylvester_polynomial_info(poly);
	auto eigensolution = sylvester_polynomial_eigensolution(poly, coeff_info, 7);
	auto rootlist = Roots::from_eigenvectors(eigensolution,
			std::numeric_limits<float>::epsilon());

	auto roots = rootlist.root_pairs(first_poly, second_poly, poly,
			std::numeric_limits<float>::epsilon());

	EXPECT_THAT(roots, UnorderedElementsAre(
		Pair(DoubleNear(-2.4641, 1e-5), DoubleNear(-1.10874, 1e-5)),
		Pair(DoubleNear(4.4641, 1e-5), DoubleNear(-0.163986, 1e-5))
	));
}

TEST(Eigensolution, ExampleSolution3)
{
	auto first_poly = BivariatePolynomial::parse("y*x -x + y + 1");
	auto second_poly = BivariatePolynomial::parse("y*x^2 - x^2 + 2*y*x + y + x + 1");

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);
	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	auto coeff_info = sylvester_polynomial_info(poly);
	auto eigensolution = sylvester_polynomial_eigensolution(poly, coeff_info, 7);
	auto rootlist = Roots::from_eigenvectors(eigensolution,
			std::numeric_limits<float>::epsilon());

	auto roots = rootlist.root_pairs(first_poly, second_poly, poly,
			std::numeric_limits<float>::epsilon());

	EXPECT_THAT(roots, ElementsAre(Pair(0, -1)));
}

TEST(Eigensolution, SymmetricExampleSolution3)
{
	auto first_poly = BivariatePolynomial::parse("x*y -y + x + 1");
	auto second_poly = BivariatePolynomial::parse("x*y^2 - y^2 + 2*x*y + x + y + 1");

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);
	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	auto coeff_info = sylvester_polynomial_info(poly);
	auto eigensolution = sylvester_polynomial_eigensolution(poly, coeff_info, 7);
	auto rootlist = Roots::from_eigenvectors(eigensolution,
			std::numeric_limits<float>::epsilon());

	auto roots = rootlist.root_pairs(first_poly, second_poly, poly,
			std::numeric_limits<float>::epsilon());

	EXPECT_THAT(roots, ElementsAre(Pair(-1, 0)));
}

TEST(Eigensolution, InterpolationIntegration1)
{
	vector<pair<double, double>> first_dataset =
	{
		{-1, 0}, {4, -1}, {-1, -5}, {-4, 2}, {4, -4},
	};

	vector<pair<double, double>> second_dataset =
	{
		{-1, -5}, {-3, 3}, {-4, -4}, {-3, -1}, {-2, -2}
	};

	auto first_poly = BivariatePolynomial::from_interpolation(2, first_dataset);
	auto second_poly = BivariatePolynomial::from_interpolation(2, second_dataset);

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);
	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	auto coeff_info = sylvester_polynomial_info(poly);
	auto eigensolution = sylvester_polynomial_eigensolution(poly, coeff_info, 7);
	auto rootlist = Roots::from_eigenvectors(eigensolution,
			std::numeric_limits<float>::epsilon());

	auto roots = rootlist.root_pairs(first_poly, second_poly, poly,
			std::numeric_limits<float>::epsilon());

	EXPECT_THAT(roots, ElementsAre(
		Pair(DoubleNear(-5.013991499, 1e-5), DoubleNear(-7.691261965, 1e-5)),
		Pair(DoubleNear(-1, 1e-5), DoubleNear(-5, 1e-5))
	));
}

TEST(Eigensolution, InterpolationIntegration2)
{
	vector<pair<double, double>> first_dataset =
	{
		{-1, -5}, {-1, 0}, {-3, 2}, {3, 3}, {-1, -3}
	};

	vector<pair<double, double>> second_dataset =
	{
		{-5, 3}, {2, 4}, {5, 1}, {-3, 1}, {-3, 2}
	};

	auto first_poly = BivariatePolynomial::from_interpolation(2, first_dataset);
	auto second_poly = BivariatePolynomial::from_interpolation(2, second_dataset);

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);
	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	auto coeff_info = sylvester_polynomial_info(poly);
	auto eigensolution = sylvester_polynomial_eigensolution(poly, coeff_info, 7);
	auto rootlist = Roots::from_eigenvectors(eigensolution,
			std::numeric_limits<float>::epsilon());

	auto roots = rootlist.root_pairs(first_poly, second_poly, poly,
			std::numeric_limits<float>::epsilon());

	EXPECT_THAT(roots, UnorderedElementsAre(
		Pair(DoubleNear(-2.999999993, 1e-5), DoubleNear(1.999999991, 1e-5)),
		Pair(DoubleNear(1.451612903, 1e-5), DoubleNear(2.741935489, 1e-5))
	));
}

TEST(Eigensolution, InterpolationIntegration3)
{
	vector<pair<double, double>> first_dataset =
	{
		{-5, 1}, {-5, 0}, {2, -2}, {-5, -2}, {4, 4}
	};

	vector<pair<double, double>> second_dataset =
	{
		{-3, -4}, {0, 4}, {-5, 0}, {1, -5}, {-4, 3}
	};

	auto first_poly = BivariatePolynomial::from_interpolation(2, first_dataset);
	auto second_poly = BivariatePolynomial::from_interpolation(2, second_dataset);

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);
	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	auto coeff_info = sylvester_polynomial_info(poly);
	auto eigensolution = sylvester_polynomial_eigensolution(poly, coeff_info, 7);
	auto rootlist = Roots::from_eigenvectors(eigensolution,
			std::numeric_limits<float>::epsilon());

	auto roots = rootlist.root_pairs(first_poly, second_poly, poly,
			std::numeric_limits<float>::epsilon());

	EXPECT_THAT(roots, UnorderedElementsAre(
		Pair(DoubleNear(-5, 1e-5), DoubleNear(0.17454e-5, 1e-5)),
		// Y value is probably correct, because the returned value is -6.55986e-14

		Pair(DoubleNear(-5, 1e-5), DoubleNear(0.4010491546, 1e-5)),
		Pair(DoubleNear(3.098181801, 1e-5), DoubleNear(1.294545405, 1e-5)),
		Pair(DoubleNear(.999999991, 1e-5), DoubleNear(-5.000000027, 1e-5))
	));
}

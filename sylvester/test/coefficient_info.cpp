#include "gtest_util.hpp"

#include "eigensolution.hpp"

using namespace softdev;
using namespace Eigen;
using namespace testing;

TEST(CoefficientInfo, Example1)
{
	MatrixXd M(3, 3);
	M <<
		2,1, 0,
		0,2, 1,
		1,0,-1;

	SylvesterPolynomial foo = {M};

	auto info = sylvester_polynomial_info(foo);

	EXPECT_THAT(info.max_coefficient(), DoubleNear(2.53209, 1e-5));
	EXPECT_THAT(info.min_coefficient(), DoubleNear(0.879385, 1e-5));
	EXPECT_THAT(info.kappa(), DoubleNear(2.87939, 1e-5));
}

TEST(CoefficientInfo, Example2)
{
	MatrixXd M(3, 3);
	M <<
		1,1,0,
		0,1,1,
		1,2,1;

	SylvesterPolynomial foo = {M};

	auto info = sylvester_polynomial_info(foo);

	EXPECT_THAT(info.max_coefficient(), DoubleNear(2.61803, 1e-5));
	EXPECT_THAT(info.min_coefficient(), DoubleNear(0, 1e-5));
	EXPECT_THAT(info.kappa(), Gt(1e15));
}

TEST(CoefficientInfo, Example3)
{
	MatrixXd M(4, 4);
	M <<
		0, 0,-1, 0,
		0, 0, 0,-1,
		0, 0, 5, 0,
		0, 0, 0, 5;

	SylvesterPolynomial foo = {M};

	auto info = sylvester_polynomial_info(foo);

	EXPECT_THAT(info.max_coefficient(), 5);
	EXPECT_THAT(fabs(info.min_coefficient()), 0);
	EXPECT_THAT(info.kappa(), Eq(std::numeric_limits<double>::infinity()));
}

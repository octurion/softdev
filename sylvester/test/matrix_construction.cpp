#include "gtest_util.hpp"

#include "eigensolution.hpp"
#include "sylvester.hpp"

using namespace softdev;
using namespace Eigen;
using namespace testing;

TEST(MatrixConstruction, CompanionMatrixExample)
{
	auto first_poly = BivariatePolynomial::parse("1 + 2x + yx^2 + 5y^1x^3 + 36y^2x^3");
	auto second_poly = BivariatePolynomial::parse("-3 + 9y + 4x^2 + 64y^2");

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);

	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	MatrixXd M_d(5, 5);
	M_d <<
		36, 0, 0, 0, 0,
		0, 36, 0, 0, 0,
		0, 0, 64, 0, 0,
		0, 0, 0, 64, 0,
		0, 0, 0, 0, 64;

	EXPECT_THAT(poly.degree(), Eq(2u));
	EXPECT_THAT(poly.max_degree_mat(), Eq(M_d));

	MatrixXd companion_matrix = softdev::companion_matrix_of(poly);
	MatrixXd expected(10, 10);
	expected <<
			  0,      0,         0,         0,         0,        1,         0,         0,        0,        0,
			  0,      0,         0,         0,         0,        0,         1,         0,        0,        0,
			  0,      0,         0,         0,         0,        0,         0,         1,        0,        0,
			  0,      0,         0,         0,         0,        0,         0,         0,        1,        0,
			  0,      0,         0,         0,         0,        0,         0,         0,        0,        1,
			  0,      0,-0.0555556,-0.0277778,         0,-0.138889,-0.0277778,         0,        0,        0,
			  0,      0,         0,-0.0555556,-0.0277778,        0, -0.138889,-0.0277778,        0,        0,
		-0.0625,      0,  0.046875,         0,         0,        0,         0, -0.140625,        0,        0,
			  0,-0.0625,         0,  0.046875,         0,        0,         0,         0,-0.140625,        0,
			  0,      0,   -0.0625,         0,  0.046875,        0,         0,         0,        0,-0.140625;

	EXPECT_THAT(companion_matrix.rows(), Eq(expected.rows()));
	EXPECT_THAT(companion_matrix.cols(), Eq(expected.cols()));
	EXPECT_THAT(companion_matrix, MatrixApproximateTo(expected, 1e-7));
}

TEST(MatrixConstruction, GeneralizedEigenproblemExample)
{
	auto first_poly = BivariatePolynomial::parse("1 + 2x + yx^2 + 5yx^3 + y^2x^3");
	auto second_poly = BivariatePolynomial::parse("-3 + 9y + 4x^2 + 1.0e14y^2x");

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);

	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	MatrixXd M_d(5, 5);
	M_d <<
		1,      0,      0,      0, 0,
		0,      1,      0,      0, 0,
		0, 1.0e14,      0,      0, 0,
		0,      0, 1.0e14,      0, 0,
		0,      0,      0, 1.0e14, 0;

	EXPECT_THAT(poly.degree(), Eq(2));
	EXPECT_THAT(poly.max_degree_mat(), Eq(M_d));

	MatrixXd l_0;
	MatrixXd l_1;
	std::tie(l_1, l_0) = softdev::generalized_eigenproblem_matrices_of(poly);

	MatrixXd expected_l_0(10, 10);
	expected_l_0 <<
		0, 0, 0, 0, 0,-1, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0,-1, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0,-1, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,-1, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0,-1,
		0, 0, 2, 1, 0, 5, 1, 0, 0, 0,
		0, 0, 0, 2, 1, 0, 5, 1, 0, 0,
		4, 0,-3, 0, 0, 0, 0, 9, 0, 0,
		0, 4, 0,-3, 0, 0, 0, 0, 9, 0,
		0, 0, 4, 0,-3, 0, 0, 0, 0, 9;

	MatrixXd expected_l_1(10, 10);
	expected_l_1 <<
		-1,    0,     0,     0,     0,     0,     0,     0,     0,     0,
		0,    -1,     0,     0,     0,     0,     0,     0,     0,     0,
		0,     0,    -1,     0,     0,     0,     0,     0,     0,     0,
		0,     0,     0,    -1,     0,     0,     0,     0,     0,     0,
		0,     0,     0,     0,    -1,     0,     0,     0,     0,     0,
		0,     0,     0,     0,     0,    -1,     0,     0,     0,     0,
		0,     0,     0,     0,     0,     0,    -1,     0,     0,     0,
		0,     0,     0,     0,     0,     0,-1e+14,     0,     0,     0,
		0,     0,     0,     0,     0,     0,     0,-1e+14,     0,     0,
		0,     0,     0,     0,     0,     0,     0,     0,-1e+14,     0;

	EXPECT_THAT(l_0, Eq(expected_l_0));
	EXPECT_THAT(l_1, Eq(expected_l_1));
}

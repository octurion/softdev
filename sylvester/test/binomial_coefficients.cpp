#include "gtest_util.hpp"

#include <array>
#include "binomial.hpp"

using softdev::BinomialCoefficients;
using namespace testing;

namespace
{
	std::array<std::array<uint64_t,6>,6> values {{
		{{1, 0,  0,  0, 0, 0}},
		{{1, 1,  0,  0, 0, 0}},
		{{1, 2,  1,  0, 0, 0}},
		{{1, 3,  3,  1, 0, 0}},
		{{1, 4,  6,  4, 1, 0}},
		{{1, 5, 10, 10, 5, 1}},
	}};
}

TEST(BinomialCoefficients, Zero)
{
	BinomialCoefficients coeff = BinomialCoefficients::up_to(0);

	EXPECT_THAT(coeff.num(), Eq(0));

	EXPECT_THAT(coeff(0, 0), Eq(1));
}

TEST(BinomialCoefficients, One)
{
	BinomialCoefficients coeff = BinomialCoefficients::up_to(1);

	EXPECT_THAT(coeff.num(), Eq(1));

	EXPECT_THAT(coeff(0, 0), Eq(1));
	EXPECT_THAT(coeff(1, 0), Eq(1));
	EXPECT_THAT(coeff(1, 1), Eq(1));
}

TEST(BinomialCoefficients, Five)
{
	BinomialCoefficients coeff = BinomialCoefficients::up_to(5);

	EXPECT_THAT(coeff.num(), Eq(5));

	for (size_t i = 0; i < values.size(); i++) {
		for (size_t j = 0; j <= i; j++) {
			EXPECT_THAT(coeff(i, j), Eq(values[i][j]));
		}
	}
}

TEST(BinomialCoefficients, OutOfBounds)
{
	BinomialCoefficients coeff = BinomialCoefficients::up_to(3);

	EXPECT_ANY_THROW(coeff(4, 3));
	EXPECT_ANY_THROW(coeff(2, 3));
}

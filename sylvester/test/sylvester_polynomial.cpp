#include "gtest_util.hpp"

#include "binomial.hpp"
#include "sylvester.hpp"

using namespace Eigen;
using namespace softdev;

using namespace testing;

TEST(SylvesterPolynomial, Cpp11InitializerList)
{
	MatrixXd M_1(3, 3);
	M_1 <<
		2, 1, 0,
		0, 2, 1,
		1, 0, -1;

	MatrixXd M_0(3, 3);
	M_0 <<
		0, -3, 0,
		0, 0, -3,
		0, 4, 5;

	SylvesterPolynomial poly = {M_0, M_1};

	EXPECT_THAT(poly.degree(), Eq(1u));
	EXPECT_THAT(poly.dimension(), Eq(3));

	EXPECT_THAT(poly[0], Eq(M_0));
	EXPECT_THAT(poly[1], Eq(M_1));
}

TEST(SylvesterPolynomial, VariableChange)
{
	MatrixXd M_1(3, 3);
	M_1 <<
		2, 1, 0,
		0, 2, 1,
		1, 0, -1;

	MatrixXd M_0(3, 3);
	M_0 <<
		0, -3, 0,
		0, 0, -3,
		0, 4, 5;

	MatrixXd expected_M_1(3, 3);
	expected_M_1 <<
		14, 1, 0,
		0, 14, 1,
		7, 8, 3;

	MatrixXd expected_M_0(3, 3);
	expected_M_0 <<
		14, -23, 0,
		0, 14, -23,
		7, 40, 43;

	SylvesterPolynomial poly = { M_0, M_1 };
	SylvesterPolynomial expected_poly = { expected_M_0, expected_M_1 };

	auto result = poly.var_change(7, 7, 2, 10);

	EXPECT_THAT(poly.degree(), Eq(expected_poly.degree()));
	EXPECT_THAT(result[0], Eq(expected_poly[0]));
	EXPECT_THAT(result[1], Eq(expected_poly[1]));
}

TEST(SylvesterPolynomial, VariableChangeWhereAllParametersAreTheSame)
{
	MatrixXd M_1(3, 3);
	M_1 <<
		2, 1, 0,
		0, 2, 1,
		1, 0, -1;

	MatrixXd M_0(3, 3);
	M_0 <<
		0, -3, 0,
		0, 0, -3,
		0, 4, 5;

	SylvesterPolynomial poly = { M_0, M_1 };
	auto result = poly.var_change(9, 9, 9, 9);

	EXPECT_THAT(result[0], Eq(result[1]));
}

TEST(SylvesterPolynomial, Evaluation)
{
	MatrixXd M_2(3, 3);
	M_2 <<
		1, 2, 3,
		4, 5, 6,
		7, 8, 9;

	MatrixXd M_1(3, 3);
	M_1 <<
		2, 1, 0,
		0, 2, -1,
		1, 0, -1;

	MatrixXd M_0(3, 3);
	M_0 <<
		0, -3, 0,
		0, 0, -3,
		0, 4, 5;

	MatrixXd expected(3, 3);
	expected <<
		8, 7, 12,
		16, 24, 19,
		30, 36, 39;

	SylvesterPolynomial poly = {M_0, M_1, M_2};

	EXPECT_THAT(poly.evaluate_for(2), Eq(expected));
}

TEST(SylvesterPolynomial, FromTwoPolynomialsExample1)
{
	auto first_poly = BivariatePolynomial::parse("2yx+y-3");
	auto second_poly = BivariatePolynomial::parse("yx^2+4x-y+5");

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);

	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	MatrixXd M_0(3, 3);
	M_0 <<
		0, -3, 0,
		0, 0, -3,
		0, 4, 5;

	MatrixXd M_1(3, 3);
	M_1 <<
		2, 1, 0,
		0, 2, 1,
		1, 0, -1;

	EXPECT_THAT(poly.degree(), Eq(1u));
	EXPECT_THAT(poly.dimension(), Eq(3u));
	EXPECT_THAT(poly[0], Eq(M_0));
	EXPECT_THAT(poly[1], Eq(M_1));
}

TEST(SylvesterPolynomial, FromTwoPolynomialsExample2)
{
	auto first_poly = BivariatePolynomial::parse("3yx + y - 2");
	auto second_poly = BivariatePolynomial::parse("yx^2 + 9x + y - 5");

	auto matrix = SylvesterMatrix::from_polynomials(first_poly, second_poly);

	auto poly = SylvesterPolynomial::from_sylvester_matrix(matrix);

	MatrixXd M_0(3, 3);
	M_0 <<
		0, -2, 0,
		0, 0, -2,
		0, 9, -5;

	MatrixXd M_1(3, 3);
	M_1 <<
		3, 1, 0,
		0, 3, 1,
		1, 0, 1;

	EXPECT_THAT(poly.degree(), Eq(1));
	EXPECT_THAT(poly.dimension(), Eq(3));
	EXPECT_THAT(poly[0], Eq(M_0));
	EXPECT_THAT(poly[1], Eq(M_1));
}

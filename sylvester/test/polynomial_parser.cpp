#include "gtest_util.hpp"

#include "polynomial.hpp"

using namespace softdev;
using namespace testing;

TEST(PolynomialParser, WithMultiplicationSymbols)
{
	auto polynomial = BivariatePolynomial::parse("2*x+3*y*x");

	EXPECT_THAT(polynomial(1, 0), Eq(2));
	EXPECT_THAT(polynomial(1, 1), Eq(3));

	EXPECT_THAT(polynomial.x_rank(), Eq(1));
	EXPECT_THAT(polynomial.y_rank(), Eq(1));
}

TEST(PolynomialParser, WithMultiplicationSymbolsAndDegrees)
{
	auto polynomial = BivariatePolynomial::parse("1+2*xy+8*y^2*x^3");

	EXPECT_THAT(polynomial(0, 0), Eq(1));
	EXPECT_THAT(polynomial(1, 1), Eq(2));
	EXPECT_THAT(polynomial(3, 2), Eq(8));

	EXPECT_THAT(polynomial.x_rank(), Eq(3));
	EXPECT_THAT(polynomial.y_rank(), Eq(2));
}

TEST(PolynomialParser, Whitespace)
{
	auto polynomial = BivariatePolynomial::parse(" 2  x *  y + 3 x y ^  2   ");

	EXPECT_THAT(polynomial(1, 1), Eq(2));
	EXPECT_THAT(polynomial(1, 2), Eq(3));

	EXPECT_THAT(polynomial.x_rank(), Eq(1));
	EXPECT_THAT(polynomial.y_rank(), Eq(2));
}

TEST(PolynomialParser, DecimalsAndExponentialForm)
{
	auto polynomial = BivariatePolynomial::parse("1.0+2.0e2*xy+8e+02*y^2*x^3+7.5x^2");

	EXPECT_THAT(polynomial(0, 0), Eq(1));
	EXPECT_THAT(polynomial(1, 1), Eq(200));
	EXPECT_THAT(polynomial(3, 2), Eq(800));
	EXPECT_THAT(polynomial(2, 0), Eq(7.5));

	EXPECT_THAT(polynomial.x_rank(), Eq(3));
	EXPECT_THAT(polynomial.y_rank(), Eq(2));
}

TEST(PolynomialParser, AdditionOfSameExponents)
{
	auto polynomial = BivariatePolynomial::parse("x^3 + 3x^3 - 5.0x^3");

	EXPECT_THAT(polynomial(3, 0), Eq(-1));

	EXPECT_THAT(polynomial.x_rank(), Eq(3));
	EXPECT_THAT(polynomial.y_rank(), Eq(0));
}

TEST(PolynomialParser, LeadingNegativeSign)
{
	auto polynomial = BivariatePolynomial::parse("-1-2x-3y-4x^2-5xy-6y^2");

	EXPECT_THAT(polynomial(0, 0), Eq(-1));
	EXPECT_THAT(polynomial(1, 0), Eq(-2));
	EXPECT_THAT(polynomial(0, 1), Eq(-3));
	EXPECT_THAT(polynomial(2, 0), Eq(-4));
	EXPECT_THAT(polynomial(1, 1), Eq(-5));
	EXPECT_THAT(polynomial(0, 2), Eq(-6));

	EXPECT_THAT(polynomial.x_rank(), Eq(2));
	EXPECT_THAT(polynomial.y_rank(), Eq(2));
}

TEST(PolynomialParser, ZeroExponents)
{
	auto polynomial = BivariatePolynomial::parse("x + 1y - 2x + 0x^2 + 3*y + 9x^2y^2 -10*y^2*x + 99 * y^0 * x^0");

	EXPECT_THAT(polynomial(0, 0), Eq(99));
	EXPECT_THAT(polynomial(1, 0), Eq(-1));
	EXPECT_THAT(polynomial(0, 1), Eq(4));
	EXPECT_THAT(polynomial(2, 0), Eq(0));
	EXPECT_THAT(polynomial(2, 2), Eq(9));
	EXPECT_THAT(polynomial(1, 2), Eq(-10));

	EXPECT_THAT(polynomial.x_rank(), Eq(2));
	EXPECT_THAT(polynomial.y_rank(), Eq(2));
}

TEST(PolynomialParser, SyntaxError1)
{
	EXPECT_ANY_THROW(BivariatePolynomial::parse("x + 4.20y + 9,5x"));
}

TEST(PolynomialParser, SyntaxError2)
{
	EXPECT_ANY_THROW(BivariatePolynomial::parse("x + 4.20^"));
}

TEST(PolynomialParser, SyntaxError3)
{
	EXPECT_ANY_THROW(BivariatePolynomial::parse("nan + 32x + inf y"));
}

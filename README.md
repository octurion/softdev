# 2015-2016 Software development project (Completed)

## Created by Alexandros Tasos (sdi1100085) and Sotiris Ntanavaras (sdi1100137)

Just an FYI, we've also done the bonus part.

### How to compile

1. Make sure that CMake 2.8.11 is installed on your machine, as well as a decent
   C++11 compiler (gcc 4.8 or clang 3.3 will suffice).

2. Ensure that you have the following packages installed under Ubuntu:
   - `git`
   - `cvs`
   - `g++`
   - `g++-4.8`
   - `cmake`
   - `automake`
   - `libtool`
   - `valgrind`
   - `libblas-dev`
   - `liblapack-dev`
   - `libeigen3-dev`
   - `qt5-default`
   - `qttools5-dev-tools`
   - `libqt5gui5`
   - `libqt5network5`
   - `libqt5printsupport5`
   - `libqt5svg5`
   - `qtbase5-dev`
   - `qtdeclarative5-dev`
   - `qtscript5-dev`
   - `qtquick1-5-dev`
   - `libqt5svg5-dev`
   - `libqt5webkit5-dev`

   (It's possible that you don't need to install some of these packages)
3. Go to the source directory and execute the following:

```
mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make
```

The CMake script will download, patch and compile Gnuplot, as well as download
Googletest for unit testing. Building takes approximately 6 minutes in our
buildbot.

(Yes, you can just execute `cmake .. && make` but this will turn the entire
directory structure into a mess. It's better to just do an out-of source
build.)

We can guarantee that the instructions presented here work, becuase we have
set up a GitLab CI server/buildbot on the Okeanos grnet cloud service that
runs the above procedure every time we git push to the server.

### How to run

Descend into `build/`, because that's where CMake puts the executable. The
executable is named `softdev`.

### How to use

Just enter the two polynomials and click "Solve". Or click "Browse" to enter
them. Or click "Interpolate" to perform an interpolation. Then click "Visualize
results" to view the two graphs, as well as the solutions.

### Running the testcases

- cd into `build/`
- Run `ctest -VV -D ExperimentalMemCheck` (to also run the tests under Valgrind)

### Issues/workarounds

- You may need to resize the Gnuplot, related windows, otherwise expect to
  observe a lot of flickering that only goes away when you resize the
  window.
- When displaying the two polynomials and the solutions, the solutions go
  out of bounds in the graph. This is not much of an issue, though, because
  the graph will contain all points initially.

### Source code hierarchy

- All Qt-related files are located at the root of the project (because, like
  QMake, that's where Qt's CMake build script expects them to be).
- The patch file `qt_gnuplot.patch` is used to patch the Gnuplot source code.
  To obtain this patch, we applied the original patch, then performed all
  changes necessary and created a new patch with `cvs diff`.
- `.gitlab-ci.yml` is the Yaml file used to build the code and run the testcases
  on the buildbot.
- `sylvester/` is the root directory for the library that parses bivariate
  polynomials, solves systems of two bivariate polynomials and outputs their
  solutions. The hierarchy is as follows:
  - `include/sylvester`: Header files. CMake exports the `include/` directory
    as an include directory, which lets us type, for instance `#include
    "sylvester/polynomial.hpp"`.
  - `test`: Testcase files. We use Googletest/Googlemock for unit testing.
  - `valgrind.supp`: Valgrind suppression file for unit testing (because
    `ld.so` has a "still reachable memory leak" on our local machine).
  - `src/`: All source code files.

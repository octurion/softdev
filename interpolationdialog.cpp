#ifndef GNUPLOT_PATH
# define GNUPLOT_PATH "gnuplot"
#endif

#include <sstream>
#include <vector>
#include <utility>

#include <QDialogButtonBox>

#include "interpolationdialog.h"
#include "ui_interpolationdialog.h"

using std::pair;
using std::stringstream;
using std::vector;

InterpolationDialog::InterpolationDialog(QWidget *parent)
	: QDialog(parent)
	, ui(new Ui::InterpolationDialog)
	, instance(new QtGnuplotInstance(nullptr, GNUPLOT_PATH))
{
    ui->setupUi(this);

	QObject::connect(ui->removePointButton, &QPushButton::clicked,
		ui->pointsList, [=] () {
			qDeleteAll(ui->pointsList->selectedItems());
			solveInterpolation();
			redrawPlot();
		});

	QObject::connect(ui->removeAllButton, &QPushButton::clicked,
		ui->pointsList, [=] () {
			ui->pointsList->clear();
			solveInterpolation();
			redrawPlot();
		});

	QObject::connect(this, &InterpolationDialog::interpolationChanged,
			ui->interpolationStatusLabel, [this] (bool has_solution) {
				ui->interpolationStatusLabel->setText(has_solution
					? tr("Interpolation instance solved!")
					: tr("Interpolation instance unsolvable"));
			});

	ui->buttonBox->button(QDialogButtonBox::Ok)->setDisabled(true);

	QObject::connect(this, &InterpolationDialog::interpolationChanged,
			ui->buttonBox->button(QDialogButtonBox::Ok), &QDialogButtonBox::setEnabled);

	ui->widget->installEventFilter(this);
    ui->widget->setStatusLabelActive(true);
    ui->widget->setAntialias(true);

	instance->setWidget(ui->widget);

	poly_degree = ui->polynomialDegree->value();

	x_axis = ui->xAxis->value();
	y_axis = ui->yAxis->value();

	emit interpolationChanged(false);
	emit graphModified();
}

void InterpolationDialog::setXAxis(int value)
{
	x_axis = value;

	emit graphModified();
}

void InterpolationDialog::setYAxis(int value)
{
	y_axis = value;

	emit graphModified();
}

bool InterpolationDialog::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() != QEvent::MouseButtonPress || obj != ui->widget) {
		return QObject::eventFilter(obj, event);
    }

	QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
	if (mouseEvent->button() != Qt::LeftButton) {
		return QObject::eventFilter(obj, event);
	}

	const auto& point_str = ui->widget->getStatusLabel()->text();

	// Crappy workaround to prevent inputting the same point more than
	// once
	for (int i = 0; i < ui->pointsList->count(); i++) {
		if (ui->pointsList->item(i)->text() == point_str) {
			return QObject::eventFilter(obj, event);
		}
	}

	ui->pointsList->addItem(point_str);

	emit graphModified();
	solveInterpolation();

	return QObject::eventFilter(obj, event);
}

InterpolationDialog::~InterpolationDialog()
{
	delete instance;

    delete ui;
}

void InterpolationDialog::solveInterpolation()
{
	vector<pair<double, double>> points;

	for (int i = 0; i < ui->pointsList->count(); i++) {
		double x;
		double y;

		QString copy = ui->pointsList->item(i)->text();

		char unused_comma;
		QTextStream stream(&copy);

		// Removing this will break the app for locales that don't use a dot
		// as a decimal separator.
		stream.setLocale(QLocale::C);
		stream.skipWhiteSpace();
		stream >> x;
		stream >> unused_comma;
		stream.skipWhiteSpace();
		stream >> y;

		points.emplace_back(x, y);
	}

	try {
		auto poly = softdev::BivariatePolynomial::from_interpolation(
				ui->polynomialDegree->value(),
				points);

		this->poly = std::move(poly);
		emit interpolationChanged(true);

	} catch (const std::exception& e) {
		this->poly = softdev::BivariatePolynomial();
		emit interpolationChanged(false);
	}
}

void InterpolationDialog::setDegree(int new_degree)
{
	poly_degree = new_degree;
	solveInterpolation();
}

void InterpolationDialog::redrawPlot()
{
	*instance << "reset\n";

	stringstream str;
	str <<
		"set tics scale 0.75\n"
		"set xtics 1\n"
		"set ytics 1\n"
		"set xrange [-" << x_axis << ":" << x_axis << "]\n"
		"set yrange [-" << y_axis << ":" << y_axis << "]\n"
		"set xlabel 'x'\n"
		"set ylabel 'y'\n"
		"set zeroaxis\n"
		"plot \"<echo '0 0'\" notitle\n";

	for(int i = 0; i < ui->pointsList->count(); i++) {
		const auto& item = ui->pointsList->item(i)->text();

		str << "replot \"<echo '" << item.toStdString() << "'\" w p ls 1 notitle\n";
	}

    *instance << str.str().c_str();
}

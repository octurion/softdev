#ifndef GNUPLOT_PATH
# define GNUPLOT_PATH "gnuplot"
#endif

#include <numeric>
#include <sstream>
#include <utility>

#include "graphvisualizer.h"
#include "ui_graphvisualizer.h"

using std::stringstream;

GraphVisualizer::GraphVisualizer(QWidget *parent)
	: QDialog(parent)
    , ui(new Ui::GraphVisualizer)
	, instance(new QtGnuplotInstance(nullptr, GNUPLOT_PATH))
	, firstPoly("0")
	, secondPoly("0")
{
    ui->setupUi(this);

	ui->widget->installEventFilter(this);
    ui->widget->setStatusLabelActive(true);
    ui->widget->setAntialias(true);

	instance->setWidget(ui->widget);

	redrawGraph();
}

GraphVisualizer::~GraphVisualizer()
{
    delete ui;
}

void GraphVisualizer::setPolynomialsAndPoints(
		std::string firstStr,
		std::string secondStr,
		std::vector<std::pair<double, double>> vec)
{
	firstPoly = std::move(firstStr);
	secondPoly = std::move(secondStr);
	points = std::move(vec);

	std::pair<double, double> max_xy = std::accumulate(
		points.begin(), points.end(), std::make_pair(0, 0),
		[]( const std::pair<double, double>& lhs,
			const std::pair<double, double>& rhs) -> std::pair<double, double>
		{
			return std::make_pair(
				std::max(std::abs(lhs.first), std::abs(rhs.first)),
				std::max(std::abs(lhs.second), std::abs(rhs.second))
			);
		});

	// Will also signal a graph change
	ui->xAxis->setValue(ceil(max_xy.first) + 1);
	ui->yAxis->setValue(ceil(max_xy.second) + 1);

	emit graphChanged();
}

void GraphVisualizer::redrawGraph()
{
	*instance << "reset\n";

	stringstream stream;
	stream <<
		"set xrange [-" << ui->xAxis->value() << ":" << ui->xAxis->value() <<"]\n"
		"set yrange [-" << ui->yAxis->value() << ":" << ui->yAxis->value() <<"]\n"
		"set isosamples 500,500\n"
		"f(x,y) = " << firstPoly << "\n"
		"g(x,y) = " << secondPoly << "\n"
		"set contour\n"
		"set cntrparam levels discrete 0\n"
		"set view 0,0\n"
		"unset ztics\n"
		"unset surface\n";

	for (const auto& p : points) {
		stream << "set label at " << p.first << ", " << p.second << ", 0.0 '' point pt2\n";
	}

	stream << "splot f(x,y), g(x,y)\n";

    *instance << stream.str().c_str();
}
